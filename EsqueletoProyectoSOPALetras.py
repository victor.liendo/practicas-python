#!/usr/bin/env python
# -*- coding: utf-8 -*-
#En este ejemplo, creamos un arreglo vacío de nombre “b”, de 3 filas x 2 columnas,
# de tipo string. Podemos usar 'float' o 'int'.

import numpy as np

#ASI SE DEFINE UNA FUNCION EN PYTHON. ESTA FUNCION VERIFICA QUE CADA LINEA QUE VA
#A LLENAR LA SOPA TIENE EXACTAMENTE M CARACTERES y ES DE SÓLO LETRAS
def validar_secuencia(secuencia,M):
    if len(secuencia) != M or secuencia.isalpha() == False:
        return False

#AQUI SE PIDE LAS DIMENSIONES DE LA SOPA DE LETRAS
N=int(input("Introduzca el número de FILAS de la SOPA: "))
M=int(input("Introduzca el número de columnas de la SOPA: "))

#ESTA ES LA FORMA DE DECLARAR UNA MATRIZ O ARREGLO BIDIMENSIONAL
#np.empty crea una variable de tipo arreglo, dtype='object'  indica que será de strings o
#caracteres. CADA CELDA DE LA MATRIZ SERA UNA LETRA

SOPA= np.empty((N,M), dtype='object')
secuencia=""
cont_lineas=0

#AQUI LEEMOS CADA SECUENCIA DE M LETRAS, N VECES. POR EJEMPLO, SI YO INDICO QUE N=2 y M=3
#VOY A LEER 2 SECUENCIAS O CADENAS DE CARACTERES, DE 3 CARACTERES CADA UNA

while cont_lineas < N:
    secuencia=input("Introduzca una secuencia de hasta " + str(M) + " caracteres sin espacios : ")
    if validar_secuencia(secuencia,M) == False:
        print ("Secuencia INVÁLIDA...")
    else:
        i=0
        while i < M:
            SOPA[cont_lineas,i]=secuencia[i]
            i+=1
        cont_lineas+=1

#AQUI IMPRIME EL CONTENIDO DE LA SOPA DE LETRAS
print(SOPA)
#AQUI IMPRIME EL CONTENIDO DE LA ULTIMA CELDA
print(SOPA[N-1,M-1])

#A PARTIR DE AQUI DEBES ESCRIBIR EL CODIGO PARA:
# 1.- LEER EL NRO DE PALABRAS A BUSCAR EN LA SOLA
# 2.- LEER CADA PALABRA A BUSCAR
# Recuerda que debes crear una función para verificar que dada palabra no tenga mas
# de M caracteres o letras. Puede tener menos, pero nunca mas, porque si no, no cabria
# en la SOPA ...
