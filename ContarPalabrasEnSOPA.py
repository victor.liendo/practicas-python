#!/usr/bin/env python
# -*- coding: utf-8 -*-
#En este ejemplo, creamos un arreglo vacío de nombre “b”, de 10 filas x 12 columnas
# la llenamos y la imprimimos

import numpy as np

#m=matriz,s=string,i y j coordernadas

# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos del límite de la matriz (j=M)
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_este(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and j < 12 and encontrada == True: # 12 sería sustítuido por M que es el número de columnas
        if m[i,j] != s[k]:
            encontrada=False
        else:
            print(str(i) + "/" + str(j))
            j+= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)

# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de la columna (j=0)
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_oeste(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and j > -1  and encontrada == True:
        if m[i,j] != s[k]:
            encontrada=False
        else:
            print(str(i) + "/" + str(j))
            j-= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)

# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de fila i=0
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_norte(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and i > -1  and encontrada == True:
        if m[i,j] != s[k]:
            encontrada=False
        else:
            print(str(i) + "/" + str(j))
            i-= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)

# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de fila i=N
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_sur(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and i < 10  and encontrada == True: #10 sería sustítuido por N que es el número de filas
        if m[i,j] != s[k]:
            encontrada=False
        else:
            print(str(i) + "/" + str(j))
            i+= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)


# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de fila i=0, columna j=0
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_noroeste(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and i > -1 and j > -1  and encontrada == True:
        if m[i,j] != s[k]:
            encontrada=False
        else:
            print(str(i) + "/" + str(j))
            i-= 1
            j-= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)


# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de fila i=0, columna j=M
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_noreste(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and i > -1 and j < 12  and encontrada == True: #12 sería sustítuido por M que es el número de columnas
        if m[i,j] != s[k]:
            encontrada=False
        else:
            print(str(i) + "/" + str(j))
            i-= 1
            j+= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)

# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de fila i=N, columna j=0
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_suroeste(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and i < 10 and j > -1  and encontrada == True: #12 sería sustítuido por M que es el número de columnas
        if m[i,j] != s[k]:
            encontrada=False
        else:
            print(str(i) + "/" + str(j))
            i+= 1
            j-= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)


# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de fila i=N, columna j=N
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_sureste(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and i < 10 and j < 12  and encontrada == True: #10 y 12 serían sustítuido por N y M
        if m[i,j] != s[k]:
            encontrada=False
        else:
            print(str(i) + "/" + str(j))
            i+= 1
            j+= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)


SOPA = np.empty((10,12), dtype='object')


SOPA[0] = ['m', 'a', 'r', 't', 'e', 'm', 'a', 'm', 'a', 'r', 't', 'e']
SOPA[1] = ['h', 'r', 'r', 'a', 'c', 't', 'r', 't', 'r', 'a', 'r', 'v']
SOPA[2] = ['t', 'o', 't', 't', 'r', 'c', 'r', 't', 'd', 'a', 'r', 'l']
SOPA[3] = ['s', 'r', 'a', 'e', 'e', 'n', 'e', 'a', 'e', 'a', 't', 'a']
SOPA[4] = ['h', 'r', 'a', 'n', 'a', 'n', 'a', 'n', 'o', 'a', 'e', 'l']
SOPA[5] = ['t', 'l', 'm', 'e', 'e', 'e', 'e', 'm', 'a', 'p', 'r', 'l']
SOPA[6] = ['t', 'l', 't', 'n', 'e', 'x', 'e', 'n', 'x', 'e', 'n', 't']
SOPA[7] = ['t', 'r', 'r', 'a', 'e', 'e', 'e', 'a', 's', 't', 't', 'o']
SOPA[8] = ['a', 't', 't', 'n', 'a', 'n', 't', 'n', 'r', 'r', 'r', 'l']
SOPA[9] = ['e', 'r', 'a', 'e', 't', 'r', 'a', 'a', 'a', 't', 'e', 'a']


#print(SOPA)
print("Esta es la SOPA de LETRAS")
i=0
j=0
while i <= 9:
    j=0
    while j <= 11:
        print (SOPA[i,j],end="\t")
        j+= 1
    print("")
    i+= 1


palabra=input("Introduzca palabra a buscar en la SOPA: ")
caracterinicial=palabra[0]

nro_apariciones=0
encontrada=False
i=0
while i <= 9: #9 sería sustituído por N que es el número de filas
    j=0
    while j <= 11: #11 sería sustituído por M que es el número de columnas
        if SOPA[i,j] == caracterinicial:
            if len(palabra) > 1:
                #ESTE: se deja la fila fija y se incrementa la columna
                print(str(i) + "/" + str(j))
                if buscar_este(SOPA,palabra,i,j + 1)==True:
                    nro_apariciones+=1
                    print("Encontrada!!!")
                    #CASO ESPECIAL: VAMOS A MOVER j hasta el fin de la palabra encontrada
                    #j=j + len(palabra) - 1
                #OESTE : se deja la fila fija y se decrementa la columna
                print(str(i) + "/" + str(j))
                if buscar_oeste(SOPA,palabra,i, j - 1)==True:
                    nro_apariciones+=1
                    print("Encontrada!!!")
                #NORTE: se deja la columna fija y se decrementa la fila
                print(str(i) + "/" + str(j))
                if buscar_norte(SOPA,palabra,i-1,j)==True:
                    nro_apariciones+=1
                    print("Encontrada!!!")
                #SUR: se deja la columna fija y se incrementa la fila
                print(str(i) + "/" + str(j))
                if buscar_sur(SOPA,palabra,i+1,j)==True:
                    nro_apariciones+=1
                    print("Encontrada!!!")

                #NOROESTE: se decrementa tanto la fila como la columna
                print(str(i) + "/" + str(j))
                if buscar_noroeste(SOPA,palabra,i-1,j-1)==True:
                    nro_apariciones+=1
                    print("Encontrada!!!")
                #NORESTE: se decrementa la fila y se incrementa la columna
                print(str(i) + "/" + str(j))
                if buscar_noreste(SOPA,palabra,i-1,j+1)==True:
                    nro_apariciones+=1
                    print("Encontrada!!!")
                #SUROESTE: se decremeenta la columna y se incrementa la fila
                print(str(i) + "/" + str(j))
                if buscar_suroeste(SOPA,palabra,i+1,j-1)==True:
                    nro_apariciones+=1
                    print("Encontrada!!!")
                #SURESTE: se inrementa tanto la fila como la columna
                print(str(i) + "/" + str(j))
                if buscar_sureste(SOPA,palabra,i+1,j+1) == True:
                    nro_apariciones+=1
                    print("Encontrada!!!")
            else:
                nro_apariciones+=1
                print("Encontrada!!!")
            encontrada=False
        j+=1
    i+=1
print ("Nro de apariciones de la palabra " + palabra + "=" + str(nro_apariciones))

