#!/usr/bin/env python
# -*- coding: utf-8 -*-
#En este ejemplo, creamos un arreglo vacío de nombre “b”, de 3 filas x 2 columnas,
# de tipo string. Podemos usar 'float' o 'int'.

import numpy as array

def llenar_arreglo(A,N):
    cont_elementos=0
    while cont_elementos < N:
        A[cont_elementos]=input("Introduzca un NÚMERO ENTERO : ")
        cont_elementos+=1
    return A


def ordenar_arreglo(A,N):
    i=0
    while i < N - 1:
        posmenor=i
        j=i+1
        while j < N:
            if A[j] < A[posmenor]:
                posmenor=j
            j+=1
        #Los elementos del arreglo sólo se intercambian si la posicion del menor
        #que comenzó igual a i,resulta ser diferente (o mayor) al final del ciclo
        if A[posmenor] != A[i]:
            menor=A[posmenor]
            A[posmenor]=A[i]
            A[i]=menor
        i+=1
    return A

#Programa principal:
#Leer dimensiones
#declarar o crear arreglo
#Llenar arreglo
#Ordenar arreglo
N=int(input("Introduzca el TAMAÑO del ARREGLO: "))
VECTOR= array.empty((N), dtype='int')
VECTOR=llenar_arreglo(VECTOR,N)
print ("Arreglo Original: ")
print (VECTOR)
VECTOR=ordenar_arreglo(VECTOR,N)
print ("Arreglo ORDENADO por SELECCIÓN DIRECTA: ")
print (VECTOR)
