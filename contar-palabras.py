#Contar palabras
#Cuenta las palabras en una secuencia de carcateres o string. Una palabra es
#    una secuencia de 1 o mas  caracteres distintos de espacio, y está:
#     antecedida por nada (comienzo del string), o "1 o mas" espacios en blanco
#     seguida por uno o mas espacios en blanco
#    un carater como "y", "o",  "u"   que cumpla las reglas anteriores, es una
#     palabra

contadorPalabras=0
palabra=""

secuencia=input("Introduzca las palabras separadas por uno o mas espacios en blanco: ")
for letter in (secuencia):  #similar a 1) obtener la longitud, 2) Iterar hasta
                            #la longitud

    if letter == " ":
        if palabra != "":
            contadorPalabras+=1
            print (palabra)
            palabra=""
    else:
        palabra=palabra + letter

#Condicion de borde: Si el último caracter de la secuencia no es " ", la última palabra no
#fue impresa...

if palabra != "" :
            contadorPalabras+=1
            print (palabra)

print ("Cantidad de palabras encontradas: ",contadorPalabras)



