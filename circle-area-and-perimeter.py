#
# Function compopsition
# This program computes the área and the perimeter of the circle defined by
# two points. One of them is its centre, and the other is
# a ponint in its perimeter
#
# Here we use a tuple two make it possible to return more than one value.
#
import math

def print_results (results):
    (area,perimeter)=results
    print ("AREA for this circle is: ", area)
    print ("and its PERIMETER is: ", perimeter)


def calc_area_and_perimeter(radius):
    area=math.pi * (radius**2)
    perimeter=math.pi * 2 * radius
    return (area, perimeter)


def  calc_radius (x1,y1,x2,y2):
    #the radius is the distance between the two points
    radius=((x2-x1)**2 + (y2-y1)**2)**0.5
    return radius

x1=int(input("Please enter the X coordinate for point A [centre]: "))
y1=int(input("Please enter the Y coordinate for point A [centre]: "))
x2=int(input("Please enter the X coordinate for point B [perimeter]: "))
y2=int(input("Please enter the Y coordinate for point B [perimeter]: "))

r=calc_radius(x1,y1,x2,y2)
results=calc_area_and_perimeter(r)
print_results(results)
