#
# Ejemplo de un método iterativo. En este caso, se calcula la raíz cuadrada de un número
# por el método de Newton. El número de iteraciones sería infinito, por lo cual se establece una
# condición de parada basada en comparar la diferencia entre la solución de la iteración actual y la de
# la anterior, contra un factor de tolerancia.
#
# Es pseudocódigo la estructura iterativa mas adecuada sería REPETIR -----  HASTA, ya que al menos
# debe realizarse una iteración
#
#
#     solucion_actual = 1
#     Repetir
#               {solucion_anterior = solucion_actual
#               {calcular solucion_actual}
#     hasta  abs(solucion_actual - solucion_anterior) < 0.00001
#
n=int(input("Please enter the number you wnat the SQUARE ROOT for: "))
approx=1.0
while True:
    oldapprox=approx
    approx=(approx + (n / approx))/2
    print (approx)
    if abs(approx - oldapprox) < 0.00001 :
        break

print (approx)
