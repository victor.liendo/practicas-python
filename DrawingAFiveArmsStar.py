#
# Using the TURTLE MODULE to draw a STAR
#
#
import turtle
#
number_of_rotations=5                                 #This is a FIVE ARMS star
turn_angle=int(360 / 5)
ttlbgcolor=input("What color do you like background to be ?: ")
ttlcolor=input("What color do you like your LINE to be drawn ?: ")
ttlpensize=int(input("What size do you want for your LINE  border ?: "))
ttllong=int(input("How long are the STAR ARMS going to be ?: "))


wn = turtle.Screen()
wn.bgcolor(ttlbgcolor)                                # Set the window background color
wn.title("Hello, Tess! Please draw a STAR...")        # Set the window title
tess = turtle.Turtle()
tess.color(ttlcolor)                                  # Tell tess to change her color
tess.pensize(ttlpensize)                              # Tell tess to set her pen width


for i in range(number_of_rotations):
    tess.forward(ttllong)
    tess.right(turn_angle * 2) #if turn_angles is used, tess will draw an heptagon, so we multiply
                               #turn_angle by 2
wn.mainloop()
