#
# Function compopsition
# This program computes the área of the circle definde by
# two points. One of them is its centre, and the other is
# a ponint in its perimeter
#
import math

def calc_area(radius):
    area=math.pi * (radius**2)
    return area




def  calc_radius (x1,y1,x2,y2):
    #the radius is the distance between the two points
    radius=((x2-x1)**2 + (y2-y1)**2)**0.5
    return radius

x1=int(input("Please enter the X coordinate for point A [centre]: "))
y1=int(input("Please enter the Y coordinate for point A [centre]: "))
x2=int(input("Please enter the X coordinate for point B [perimeter]: "))
y2=int(input("Please enter the Y coordinate for point B [perimeter]: "))

print ("the AREA for this circle is: ", calc_area(calc_radius(x1,y1,x2,y2)))