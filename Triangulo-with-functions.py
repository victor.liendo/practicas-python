#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Dado un número N, el programa dibuja un triángulo de base y altura N
#
# Ejemplo: N=4
#
# ****
#  ***
#   **
#    *
#
def dibujar_triangulo(n,caracter_triangulo):
    linea=""
    tamano=n
    while n > 0:
        #construimos una linea con el caracter definido,de tamaño n
        linea=caracter_triangulo * n
        #la rellenamos de espacios por la izquierda hasta el tamaño del triángulo
        linea=linea.rjust(tamano)
        #la imprimimos
        print (linea)
        #decrementamos en 1 el tamaño de la línea para la próxima iteración
        n=n-1
#
#PROGRAMA PRINCIPAL
#
caracter_triangulo=input("Indique el caracter para dibujar el triángulo: ")
n=int(input("Introduzca tamaño de la BASE y ALTURA del TRIÁNGULO: "))
if n == 0:
    print ("Tamaño de base y altura debe ser mayor que 0")
else:
    dibujar_triangulo(n,caracter_triangulo)


