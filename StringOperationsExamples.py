prefixes = "JKLMNOPQRSTWXYZ"
suffix = "ack"

print ("Printing names ending in ACK:")
print ("")

for p in prefixes:
    print(p + suffix)

print ("")

print("Testing the substring operation on the string: TESTING THE SUBSTR OPR.")
a="TESTING THE SUBSTR OPR"
aSubstring = a[0:7]
print (aSubstring)

#String are immutable. You can't change an existing one, just create a new
#one by doing some tricks

print ("Replacing T for R")
b="R" + aSubstring[1:]
print (b)


print ("Checking for existence of a Z in the string...")
zExists='z' in b
print (zExists)


print ("Remove the vowels from the string: Laura la sin par de Caurimare")
vowels="AEIOUaeiou"
sentence="Laura la sin par de Caurimare"
newsentence=""
for letter in sentence:
    if not letter in vowels:
        newsentence=newsentence + letter
print (newsentence)
