#!/usr/bin/env python
# -*- coding: utf-8 -*-

def strreplace(old, new, s):
    return new.join(s.split(old))

unitsdict={}
filename="/media/victorl/vliendo/ProyectoMincoExVUCE/DATA/Archivo_ARANCEL_Marzo_2017.csv"
inputFile=open(filename,"r")
line=inputFile.readline()
while len(line) != 0:                            # Keep reading
        cod=""
        name=""
        unit=""
        unitname=""
        fields=line.split("|")
        cod=fields[0]
        max=len(fields)
        for i in range (1,max):  #Itera hasta max - 1
            if i < max - 2:
                name=name + " " + fields[i]
            else:
                if  i < max - 1:
                    unit=fields[i]
                else:
                    unitname=fields[i]
        unitname=strreplace("("," ",unitname)
        unitname=strreplace(")"," ",unitname)
        unitname=strreplace("'"," ",unitname)
        unit=strreplace("("," ",unit)
        unit=strreplace(")"," ",unit)
        unit=strreplace("'"," ",unit)
        #print("{0}-{1}-{2}-{3}".format(cod,name,unit,unitname))
        if not unit in unitsdict:
            unitsdict[unit]=unitname
        line=inputFile.readline()
inputFile.close()
for k in unitsdict.keys():
   print("Unidad de Medida {0} : {1}".format(k,unitsdict[k]))

