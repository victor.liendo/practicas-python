#
# Using the TURTLE MODULE to draw a CLOCK FACE
#
#
import turtle
import math
#

turn_angle=int(360 / 12)       # 12 because a clock has 12 main positions
ttlbgcolor=input("What color do you like background to be ?: ")
ttlcolor=input("What color do you like your LINE to be drawn ?: ")
ttlpensize=int(input("What size do you want for your LINE  border ?: "))
ttllong=int(input("How long are the clock SIDES  going to be ?: "))

clock_length = ttllong * 12    # the clock borderline covers a lenght similar
                               # to the one of a circle
clock_radius = int(clock_length / (2 * (math.pi)))
print (clock_radius)

wn = turtle.Screen()
wn.bgcolor(ttlbgcolor)                         # Set the window background color
wn.title("Hello, Tess! Please draw a CLOCK...")# Set the window title
tess = turtle.Turtle()
tess.color(ttlcolor)                           # Tell tess to change her color
tess.pensize(ttlpensize)                      # Tell tess to set her pen width
tess.shape("turtle")
tess.speed(1)
tess.penup()
tess.stamp()
tess.goto(-clock_radius,0)
tess.left(90)


for i in range(12):
    tess.stamp();
    tess.right(turn_angle)
    tess.forward(ttllong)


wn.mainloop()
