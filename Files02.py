#
# Working with files
#
#
#
import os
ready=False
while not ready:
    filename=input("Introduzca la RUTA del ARCHIVO de ENTRADA: ")
    # This is the preferred way to check if a file exists.
    if os.path.isfile(filename):
        ready=True
    else:
        print ("Archivo NO EXISTE")

try:
    inputFile=open(filename,"r")
    line=inputFile.readline()
    print("")
    print("Este es el contenido del archivo")
    print ("")
    while len(line) != 0:                            # Keep reading
        fields=line.split("|")
        for field in fields:
            print(field,end="\t")
        print ("")
        line=inputFile.readline()
    inputFile.close()
except:
    print("ERROR ABRIENDO ARCHIVO de ENTRADA !!!")
finally:
    print("")
    print("Fin del Proceso ...")

#
# Now, let's read the file'
#

