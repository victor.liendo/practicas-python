#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
import turtle
import math


def draw_koch_fractal(o,order,size):  #o is the turtle object
    if order==0:
        """Draw just a straight line, of 'size' lenght"""
        o.forward(size)
    else:
        """Divide the straight line in four slices, each of them being """
        """1/3 of the origional size"""
        draw_koch_fractal(o, order-1, size/3)
        o.left(60)
        draw_koch_fractal(o, order-1, size/3)
        o.right(120)
        draw_koch_fractal(o, order-1, size/3)
        o.left(60)
        draw_koch_fractal(o, order-1, size/3)

ttlbgcolor=input("What color do you like background to be ?: ")
ttlcolor=input("What color do you like your LINE to be drawn ?: ")
nf=int(input("Please enter the FRACTAL number (0,1,2,3,...):"))
wn = turtle.Screen()
wn.bgcolor(ttlbgcolor)                         # Set the window background color

wn.title("Hello, Tess! Please draw a FRACTAL") # Set the window title
tess = turtle.Turtle()
tess.color(ttlcolor)                           # Tell tess to change her color
tess.pensize(2)                       # Tell tess to set her pen width
#
## Desabilitar la escritura, hacer invisible al objeto trurtle
tess.penup()
tess.hideturtle()
## Colocar posición de inicio del objeto
tess.setx(-300)
tess.sety(0)
#Habilitar nuevamente el objeto
tess.pendown()
tess.showturtle()
## Dibujar el FRACTAL
draw_koch_fractal(tess,nf,600)

wn.mainloop()

