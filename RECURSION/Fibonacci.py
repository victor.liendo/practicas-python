#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

"""Esta es una variable global, por lo cual en la función fibonacci_2"""
"""es actualizada en cada llamada recursiva. En otras palabras, cada"""
"""llamada hace uso de la variable actualizada"""
known_values={0:0, 1:1}


def fibonacci(n):
    if n==0:
       return 0
    elif n==1:
        return 1
    else:
        return (fibonacci(n-1) + fibonacci(n-2))


def fibonacci_2(n):
    """Sabemos que para 0 el valor de la función es 0, para 1 es 1"""

    if n not in known_values:
        fib=fibonacci_2(n-1) + fibonacci_2(n-2)
        known_values[n]=fib

    return known_values[n]


print(fibonacci_2(10))
print("---")
for k in known_values.keys():
    print("{0}-{1}".format(k,known_values[k]))
