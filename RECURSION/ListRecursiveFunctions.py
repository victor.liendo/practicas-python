#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# IMPLEMENTACIÓN DE ALGUNAS OPERACIONES SOBRE LISTAS ...
#
# En la RECURSIÓN es importante tener en cuenta lo siguiente:
#
# Dentro de la función recursiva, puede haber una o mas llamadas a ella misma,
# pero tiene que haber un caso BASE, a través del cual, la función retorne un
# valor. De lo contario, la función se llamará INFINITAMENTE a sí misma
#
def r_suma(datos): #SUMA

    total=0
    for element in datos:
        if type(element) == type([]):
            """El elemento de la lista es una lista anidada"""
            total=total + r_suma(element)
        else:
            """CASO BASE."""
            total=total + element
    return total

def r_primer(datos): #RETORNAR PRIMER ELEMENTO

    primer=datos[0]
    if type(primer) == type([]):
        """El elemento de la lista es una lista anidada"""
        primer=r_primer(primer)

    """CASO BASE"""
    """cuando el valor recibido como parámetro de entrada ya no sea """
    """una lista anidada, no se invocara mas a la funcion de forma recursiva """
    """sino que retornara un valor"""
    return primer

def r_max(datos): #OBTENER el MAXIMO

    max=r_primer(datos)
    for elemento in datos:
        if type(elemento) != type([]):
            """CASO BASE"""
            if elemento > max:
                max=elemento
        else:
            aux=r_max(elemento)
            if aux > max:
                max=aux
    return max

def r_flat(datos): #RETORNA UNA LISTA PLANA (sin listas anidadas)
    datos_flat=[]
    for element in datos:
        if type(element) == type([]):
            datos_flat=datos_flat + r_flat(element) #La operación + concatena listas ...
        else:
            """CASO BASE."""
            datos_flat.append(element)
    return datos_flat


#La siguiente lista contiene números o listas anidadas, por tanto es un
#ejemplo de estructura de datos recursiva.

datos=[[[[10500,77],40],80],120,[25,[78,38,2017,16],89,30,60],90,70,10,[7,[21800,18],4,70],32]

print("La SUMA de los elementos de la LISTA es {0}".format(r_suma(datos)))
print("El MÁXIMO elementos de la LISTA es {0}".format(r_max(datos)))
print("La lista sin LISTAS ANIDADAS es {0}".format(r_flat(datos)))



