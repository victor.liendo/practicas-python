#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# 1) La descripción del PROBLEMA OCHO REINAS se encuentra en la versión 1
#
# 2) En esta versión, generalizamos el problema a un TABLERO de M filas y COLUMNAS
# M>=2
#
# 3) Además, no hace falta la función buscar_norte, porque con saber que la columna
# está ocupada es suficiente
#
# 4) En esta versión ademas:
#
#    a: Pedimos al usuario que ingrese un número entre 0 y M, para indicar en que
# columna quiere colocar la primera REINA en la fila 0
#
# 5) Buscamos todas las SOLUCIONES POSIBLES para la selección hecha en 4)
#
#
#
import numpy as arreglo
import time


# Vamos a iterar mientras
#    no nos salgamos de fila i=0, columna j=0 y no hayamos encontrado una REINA

def buscar_noroeste(m, i, j):
    reinaencontrada = False
    while  i > -1 and j > -1  and reinaencontrada == False:
        if m[i,j] == "R":
            reinaencontrada=True
        else:
            i-= 1
            j-= 1
    return (reinaencontrada)


# Vamos a iterar mientras
#    no nos salgamos de fila i=0, columna j=M y no hayamos encontrado una REINA

def buscar_noreste(m, i, j):
    reinaencontrada = False
    while  i > -1 and j < M  and reinaencontrada == False:
        if m[i,j] == "R":
            reinaencontrada=True
        else:
            i-= 1
            j+= 1
    return (reinaencontrada)

#
# LEEMOS LA DIMENSIÓN DEL TABLERO
#
M=int(input("Introduzca un entero para indicar el tamaño del TABLERO: "))
#
#CREAMOS LA MATRIZ QUE REPRESENTA EL TABLERO DE AJEDREZ
#
CHESS= arreglo.empty((M,M), dtype='object')
columna = int(input("Introduzca COLUMNA para REINA en 1era FILA (0 <= COLUMNA < " + str(M) + "): "))

while columna < 0 or columna > M -1:
    columna=int(input("COLUMNA fuera de rango...Introduzca otro número :"))

print ("La primera REINA va en la  FILA 0, COLUMNA " + str(columna))
t0 = time.clock()

#
#CREAMOS LOS ARREGLOS DONDE MANTENDREMOS LA SOLUCIÓN PARCIAL.
# SOLUTIONSf mantendrá las columnas ocupadas en cada fila
# Si SOLUTIONSf[0]=3 significa que en la fila 0 está ocupada la columna 3
# Si SOLUTIONSc[0]=3 significa que en la columna 0 está ocupada la fila 3
#
SOLUTIONf=arreglo.empty((M), dtype='int')
SOLUTIONc=arreglo.empty((M), dtype='int')
#
#Inicializamos TABLERO y SOLUCION con "-1" y "*"
#
for i in range(M):
    SOLUTIONf[i]=-1
    SOLUTIONc[i]=-1
    for j in range(M):
        CHESS[i,j]="*"

countoper=0
countsol=0
SOLUTIONf[0]=columna
SOLUTIONc[columna]=0

#
CHESS[0,columna]="R"
countoper+=1
#
colocada=True
i=1
while i > 0 and i < M:
    """ Si he colocado una REINA en la fila anterior,"""
    """comienzo desde la col 0 en la fila actual"""
    if colocada:
        j=0
        colocada=False
    while j < M and colocada == False:
        if not buscar_noroeste(CHESS,i,j) and not buscar_noreste(CHESS,i,j) and SOLUTIONc[j] == -1:
            """COLOCO UNA REINA"""
            CHESS[i,j]="R"
            countoper+=1
            SOLUTIONf[i]=j
            SOLUTIONc[j]=i
            colocada=True
        else:
            j+=1

    if not colocada:
        """Si no pude colocar una REINA en la FILA"""
        """me regreso a la fila anterior,elimino esa jugada"""
        """y avanzo de columna en esa fila"""
        """Descocupo la jugada en SOLUTIONf y SOLUTIONc"""
        i-=1
        CHESS[i,SOLUTIONf[i]]="*"
        countoper+=1
        SOLUTIONc[SOLUTIONf[i]]=-1
        j=SOLUTIONf[i]+1
        SOLUTIONf[i]=-1

    else:
        """ Si la pude colocar, AVANZO de FILA"""
        i+=1
        if i == M and SOLUTIONf[i-1] != -1:
            """¡¡¡ Hay una solución !!!"""
            countsol+=1
            for k in range(M):
                for l in range(M):
                    print (CHESS[k,l], end=" ")
                print("")
            print(SOLUTIONf)
            """me regreso a buscar nuevas soluciones"""
            """Haciendo lo mismo que cuando no ccoloco una REINA"""
            i-=1
            CHESS[i,SOLUTIONf[i]]="*"
            countoper+=1
            SOLUTIONc[SOLUTIONf[i]]=-1
            j=SOLUTIONf[i]+1
            SOLUTIONf[i]=-1
            colocada=False
""" EL ALGORITMO TERMINA CUANDO, HABIENDOME REGRESADO HASTA LA """
""" SEGUNDA FILA, no encuentro mas columnas donde colocar una REINA"""
""" EN ESE CASO i=0 y se saldra del LOOP"""

t1 = time.clock()
print("TIEMPO TOTAL {0:.4f} SEGUNDOS...".format(t1-t0))
print("Total de Operaciones: {0} . Total de Soluciones encontradas {1}: ".format(countoper,countsol))
