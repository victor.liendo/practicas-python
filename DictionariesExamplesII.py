

#Here we implement the "student" structure as a DICTIONARY (k, v) where
# k is the student name, and v is a TUPLE (career, subject)

students = {"Samuel Liendo": ("Computer Science",["Discrete Maths", "Physics","Algorithms","Informatics"]),
            "Paula Liendo": ("Arts",["Maths", "Latin", "French","History","English"]),
            "Andrés Aiffil": ("Engineering",["Maths", "Latin", "French","History","English"])}

students["Yolima Carvajal"]=("Computer Science",["Discrete Maths", "Physics","Algorithms","Informatics"])

mathcounter=0
isSomeoneStudyingChemistry=False;

for k in students.keys():
    (career, subjects)=students[k]
    print ("Student name: {0} \tCareer: {1}".format(k,career))
    print ("Subjects are:")
    for (i,v) in enumerate(subjects):    ## CON ESTE FOR OBTENGO CADA ELEMENTO DE LA LISTA
                                             ## COMO UN PAR (indice,valor)
        print ("\t"+ str(i) + "\t" + v)
        if v == "Maths":
            mathcounter+=1
    if "Chemistry" in subjects:
        isSomeoneStudyingChemistry=True