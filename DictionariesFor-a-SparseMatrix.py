#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Dictionaries are like Maps in JAVA. The allow us to keep a key-value
# relationship for may elements
#
#
#
sparsem={}
#
#  0 0 0 0 0 0
#  0 0 0 1 0 0
#  0 0 0 0 0 0
#  0 0 0 3 0 0
#  1 0 0 0 0 0
#  0 6 0 0 0 0
#
#En este caso, la clave del diccionario es una TUPLA
#
sparsem={(1,3):1, (3,3):3, (4,0):1, (5,1): 6}
try:
    print("El elemento en la fila 0, columna 1 es {0}".format(sparsem[(0,1)]))
except:
    print("El elemento en la fila 0, columna 1 es 0 o no existe ")

try:
    print("El elemento en la fila 5, columna 1 es {0}".format(sparsem[(5,1)]))
except:
    print("El elemento en la fila 5, columna 1 es 0 0 no existe")