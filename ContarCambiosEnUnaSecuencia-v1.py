#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#Contar cambios en una secuencia. Dada una secuencia de numeros enteros, esta
#es incremental si cada número es mayor o igual  que el anterior. Es
# decremental en caso contrario. En una secuencia como la siguiente:
# 5 6 7 4 1 2 3 6 5 0, hay tres cambios: cuando cambia de 7 a 4, cuando
# cambia de 1 a 2, y cuando cambia de 6 a 5.
# Se asume el 0 como el entero que define fin de la secuencia...Contar

tipo_secuencia=""
numero_anterior = 0
numero_cambios = 0
numero=int(input("Introduzca    primer número de la secuencia, o 0 para FINALIZAR  :"))
if numero == 0:
    print ("No se introdujo secuencia alguna. OPERACIÓN FINALIZADA")
else:
    numero_anterior=numero
    while numero != 0:
        numero=int(input("Introduzca siguiente número de la secuencia, o 0 para FINALIZAR  :"))
        if numero == 0:
            print ("Fin de Secuencia. Número de cambios encontrados: ", numero_cambios)
        else:
            if numero >= numero_anterior:
                if tipo_secuencia=="dec":
                    numero_cambios=numero_cambios + 1
                tipo_secuencia="inc"
            else:
                if tipo_secuencia=="inc":
                    numero_cambios=numero_cambios + 1
                tipo_secuencia="dec"
            numero_anterior=numero

    #SE INTRODUJO 0. FIN DE SECUENCIA
    print ("OPERACIÓN FINALIZADA")




