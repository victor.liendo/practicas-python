#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Este programa se encarga de encontrar una solución al probrema de las Ocho
# REINAS, que consiste en colocar 1 REINA en cada fila del tablero (matriz 8x8)
# sin que ninguna REINA pueda atacar a otra. Para ello:
#
# 1.- Generaremos un múmero RANDOM entre 0 y 7, para colocar una reina en
# una columna de la 1era fila de la matriz
#
# 2.- A partir de la 2da fila, desde la primera columna, buscaremos si hay
# alguna REINA en direcciones NorOESTE, NORTE y NorESTE. En caso negativo
# (NO HAY REINAS), colocamos una REINA en esa fila/columna, de lo contrario,
# avanzamos de columna ...
# Nota: no se busca en direcciones:
# -oeste
# -este
# -sur
# -sureste
# -suroeste
#.............porque el algoritmo no puede haber puesto una REINA en ellas
#
# 3.- MUESTRA LA PRIMERA SOLUCIÓN QUE ENCUENTRA


import numpy as arreglo
import random

# Vamos a iterar mientras
#    no nos salgamos de fila i=0, columna j=0 y no hayamos encontrado una REINA

def buscar_noroeste(m, i, j):
    reinaencontrada = False
    while  i > -1 and j > -1  and reinaencontrada == False:
        if m[i,j] == "R":
            reinaencontrada=True
        else:
            i-= 1
            j-= 1
    return (reinaencontrada)


# Vamos a iterar mientras
#    no nos salgamos de fila i=0 y no hayamos encontrado una REINA

def buscar_norte(m, i, j):
    reinaencontrada = False
    while i > -1  and reinaencontrada == False:
        if m[i,j] == "R":
            reinaencontrada=True
        else:
            i-= 1
    return (reinaencontrada)



# Vamos a iterar mientras
#    no nos salgamos de fila i=0, columna j=M y no hayamos encontrado una REINA

def buscar_noreste(m, i, j):
    reinaencontrada = False
    while  i > -1 and j < 8  and reinaencontrada == False:
        if m[i,j] == "R":
            reinaencontrada=True
        else:
            i-= 1
            j+= 1
    return (reinaencontrada)

#
#CREAMOS LA MATRIZ QUE REPRESENTA EL TABLERO DE AJEDREZ
#
CHESS= arreglo.empty((8,8), dtype='object')
#
#CREAMOS EL ARREGLO DONDE MANTENDREMOS LA SOLUCIÓN PARCIAL. Cada vez que
#se pueda colocar una REINA en una columna j para una fila i, el valor
#de j se guardará en la iésima posición del arreglo
#
#Ejemplo: Si en la fila 1, se pudo colocar una reina en la columna 2, entonces
#SOLUTIONS[1]=2
#
SOLUTION=arreglo.empty((8), dtype='int')
#
#Inicializamos TABLERO y SOLUCION con "-1" y "*"
#
for i in range(8):
    SOLUTION[i]=-1
    for j in range(8):
        CHESS[i,j]="*"
#
#Generamos un número RANDOM entre 0 y 7 que nos dirá en que columna colocar una R
#en la 1era FILA
#
randomgenerator = random.Random()
columna = randomgenerator.randrange(0,8)
print ("La primera REINA va en la  FILA 0, COLUMNA " + str(columna))
SOLUTION[0]=columna
#
CHESS[0,columna]="R"
#
colocada=True
i=1
while i < 8: #Desde la FILA 1 a la 7
    if colocada:                  #Si he colocado una REINA en la fila anterior,
        j=0                       #comienzo desde la col 0 en la fila actual
        colocada=False
    while j < 8 and colocada == False:
        if not buscar_noroeste(CHESS,i,j) and not buscar_norte(CHESS,i,j) and not buscar_noreste(CHESS,i,j):
            CHESS[i,j]="R"
            SOLUTION[i]=j
            colocada=True
        else:
            j+=1
    if not colocada:              #Si no pude colocar una REINA en la FILA
        i-=1                      #me regreso a la fila anterior,elimino
        CHESS[i,SOLUTION[i]]="*"  #esa solución, y avanzo de columna en esa fila
        j=SOLUTION[i]+1
        SOLUTION[i]=-1
    else:
        i+=1                      #Si la pude colocar, avanzo de FILA


#VEAMOS AHORA COMO QUEDÓ EL TABLERO

for i in range(8):
    for j in range(8):
        print (CHESS[i,j], end=" ")
    print("")


