import turtle

def draw_multicolor_square(t, sz):
    """Make turtle t draw a multi-color square of sz."""

    for i in ["red", "purple", "hotpink", "blue"]:   # THIS IS A LIST
                                                     # i takes a string value in each iteration
        t.color(i)
        t.forward(sz)
        t.left(90)

wn = turtle.Screen()        # Set up the window and its attributes
wn.bgcolor("lightgreen")

tess = turtle.Turtle()      # Create tess and set some attributes
tess.pensize(3)


size = 20                   # Size of the smallest square
for i in range(20):         # The program will draw 15 squares

    draw_multicolor_square(tess, size)    #for each step, size remains the same
                                          #,thus allowing to draw a square

    size = size + 10        # Increase the size for next time. So the next square is going
                            # to be a bit larger

    tess.forward(10)        # Move tess along a little
    tess.right(12)          #    and give her some extra turn

wn.mainloop()