#!/usr/bin/env python
# -*- coding: utf-8 -*-
#En este ejemplo, creamos un arreglo vacío de nombre “b”, de 3 filas x 2 columnas,
# de tipo string. Podemos usar 'float' o 'int'.

import numpy as np


b = np.empty((3,2), dtype='object')

b[0]=['a','b']
b[1]=['a','0']
b[2]=['c','d']

#b[2]=['c','d,'e']  #genera error porque la columna 3 no existe
#b[3]=['e','f']     #genera error porque la fila 4 no existe

print (b[0,0])      #así accedemos a un elemento