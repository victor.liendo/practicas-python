#!/usr/bin/env python
# -*- coding: utf-8 -*-
#En este ejemplo, creamos un arreglo vacío de nombre “b”, de 10 filas x 12 columnas
# la llenamos y la imprimimos

import numpy as np

#m=matriz,s=string,i y j coordernadas
def buscar_der(m, s, i, j):
    encontrada = True

# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos del límite de la matriz (j)
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

    k = 1
    while k < len(s) and j < 12 and encontrada == True: # 12 sería sustítuido por M que es el número de columnas
        if m[i,j] != s[k]:
            encontrada=False
        else:
            print(str(i) + "/" + str(j))
            j+= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)

SOPA = np.empty((10,12), dtype='object')


SOPA[0] = ['r', 'a', 't', 'o', 'n', 'c', 'o', 'l', 'l', 'a', 'r', 't']
SOPA[1] = ['h', 'a', 'm', 'a', 'c', 'a', 's', 'a', 'l', 'a', 's', 'v']
SOPA[2] = ['m', 'o', 't', 'o', 'r', 'c', 'i', 'u', 'd', 'a', 'd', 'l']
SOPA[3] = ['s', 'u', 's', 'p', 'e', 'n', 's', 'o', 's', 'a', 'l', 'a']
SOPA[4] = ['h', 'l', 'm', 'c', 'a', 's', 'a', 'h', 'o', 't', 'e', 'l']
SOPA[5] = ['e', 'l', 'm', 'e', 'a', 's', 'a', 'm', 'a', 'p', 'a', 'l']
SOPA[6] = ['t', 'l', 't', 'c', 'a', 'x', 'e', 'n', 'r', 'r', 'o', 'l']
SOPA[7] = ['r', 'r', 'm', 'a', 'r', 'a', 'c', 'a', 's', 't', 'e', 'o']
SOPA[8] = ['a', 'r', 't', 'e', 'a', 'r', 't', 'e', 'o', 's', 'o', 'l']
SOPA[9] = ['h', 'l', 'm', 'c', 'a', 's', 'a', 'h', 'o', 't', 'e', 'l']


#print(SOPA)
print("Esta es la SOPA de LETRAS",end='\n')
i=0
j=0
while i <= 9:
    j=0
    while j <= 11:
        print (SOPA[i,j],end="\t")
        j+= 1
    print("")
    i+= 1


palabra=input("Introduzca palabra a buscar en la SOPA: ")
caracterinicial=palabra[0]


encontrada=False
i=0
while i <= 9: #9 sería sustituído por N que es el número de filas
    j=0
    while j <= 11: #11 sería sustituído por M que es el número de columnas
        if SOPA[i,j] == caracterinicial:
            if len(palabra) > 1:
                print(str(i) + "/" + str(j))
                encontrada = buscar_der(SOPA, palabra, i, j + 1)
                print(encontrada)
            else:
                encontrada = True
                print(encontrada)
            encontrada=False
        j+=1

    i+=1

#print (str(encontrada) + " en posicion: " + str(i)+"/"+str(j))




