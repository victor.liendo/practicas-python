#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This is a version of the bubble sort algorithm, which stops
# if, at the end of a step, the array is orderded (LOG=False)

import numpy as array

def llenar_arreglo(A,N):
    cont_elementos=0
    while cont_elementos < N:
        A[cont_elementos]=input("Introduzca un NÚMERO ENTERO : ")
        cont_elementos+=1
    return A


def ordenar_arreglo(A,N):
    i=0
    while i < N - 1:
        j=0
        LOG=False
        while j < (N - 1) - i:
            if A[j] > A[j+1]:
                aux=A[j+1]
                A[j+1]=A[j]
                A[j]=aux
                LOG=True
            j+=1
        print ("Hemos iterado " + str(i + 1) + " veces ...")
        if LOG:
            i=i+1
        else:
            i = N - 1
    return A

#Programa principal:
#Leer dimensiones
#declarar o crear arreglo
#Llenar arreglo
#Ordenar arreglo
N=int(input("Introduzca el TAMAÑO del ARREGLO: "))
VECTOR= array.empty((N), dtype='int')
VECTOR=llenar_arreglo(VECTOR,N)
print ("Arreglo Original: ")
print (VECTOR)
VECTOR=ordenar_arreglo(VECTOR,N)
print ("Arreglo ORDENADO por BURBUJA: ")
print (VECTOR)
