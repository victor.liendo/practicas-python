#
# This is another example to deal with LISTS handling.
# In this case, we create or initializa a lists of studens, each of them
# also having a list of the subjects they have enrolled in the current semester

students = [
    ("Samuel Liendo","Computer Science",["Discrete Maths", "Physics","Algorithms","Informatics"]),
    ("Paula Liendo","Social Communication",["Maths", "Latin", "French","History","English"]),
    ("Andrés Aiffil","Engineering",["Maths", "Latin", "French","History","English"])
]

mathcounter=0
for name, career, subjects in students:
    print ("Student name: ", name)
    for subject in subjects:
        print ("\t" + subject)
        if subject == "Maths":
            mathcounter+=1

print ("Math has " + str(mathcounter) + " students enrolled " )