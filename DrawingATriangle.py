#
#
#
import turtle

ttlbgcolor=input("What color do you like for the  background ?: ")
ttlcolor=input("What color do you like your LINE to be drawn ?: ")
ttlpensize=int(input("What size do you want for your LINE  border ?: "))
ttllong=int(input("How long should the triangle base be "))
wn = turtle.Screen()
wn.bgcolor(ttlbgcolor)          # Set the window background color
wn.title("Hello, Tess! Please draw a SQUARE")        # Set the window title
tess = turtle.Turtle()
tess.color(ttlcolor)            # Tell tess to change her color
tess.pensize(ttlpensize)        # Tell tess to set her pen width
for i in range(3):
    tess.forward(ttllong)
    tess.left(120)


wn.mainloop()
