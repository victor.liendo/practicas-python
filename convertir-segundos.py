#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

hours_r=0
minutes_r=0
seconds_r = 0
while True:
	try:
		seconds=int(input("Introduzca la cantidad de segundos a convertir en h/m/s : "))
		hours_r = seconds // 3600;
		remaining_seconds = seconds % 3600;
		if remaining_seconds >= 60:
			minutes_r = remaining_seconds // 60
			seconds_r = remaining_seconds % 60
		else:
			minutes_r = 0
			seconds_r = remaining_seconds

		print ("horas : ", hours_r)
		print ("minutos: ", minutes_r)
		print ("seconds: ", seconds_r)
		break
	except ValueError:
        	print("Entrada no es un número entero. Intente de nuevo...")
print("Operación realizada exitosamente")