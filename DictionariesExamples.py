#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Dictionaries are like Maps in JAVA. The allow us to keep a key-value
# relationship for may elements
#
#
# In this example, were gonna fill a dictionary from a file on the filesystem,
# then we cank ask the user to enter a word, and get the translation
phrasal_verb_dict={}
try:
    filename="/home/victorl/Documentos/DIPLOMADO-UPEL-EducacionUniversitaria/TEMAS-o-AREAS/ALGORITMOS-PROGRAMACION/EJEMPLOS/PYTHON/SimpleDict.txt"
    inputFile=open(filename,"r")
    line=inputFile.readline()
    print ("")
    while len(line) != 0:                            # Keep reading
        fields=line.split(":")
        phrasal_verb_dict[fields[0]] = fields[1]
        line=inputFile.readline()
    inputFile.close()
    print ("Archivo Cargado en Diccionario ...")
except:
    print("ERROR ABRIENDO ARCHIVO de ENTRADA !!!")

phrasalv=input("Enter a phrasal verb, or END to finish :")
while phrasalv != "END":
        print("")
        try:
            print("\t{0}".format(phrasal_verb_dict[phrasalv]))
            print("")
        except:
            print("\tNOT FOUND")
        finally:
            phrasalv=input("Enter a phrasal verb, or END to finish :")

#Algunas operaciones sobre mapas o diccionarios

del phrasal_verb_dict["look out"]
phrasal_verb_dict["take it easy"]="Be happy, stay call, no need to rush"
print ("Palabras en el DICCIONARIO : {0}".format(len(phrasal_verb_dict)))
for k in phrasal_verb_dict.keys():
   print("PHRASAL VERB -{0}- : {1}".format(k,phrasal_verb_dict[k]))


