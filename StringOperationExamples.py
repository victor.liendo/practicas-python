prefixes = "JKLMNOPQRSTWXYZ"
suffix = "ack"

print ("Printing names ending in ACK:")
print ("")

for p in prefixes:
    print(p + suffix)

print ("")

print("Testing the substring operation on the string: TESTING THE SUBSTR OPR.")
a="TESTING THE SUBSTR OPR"
aSubstring = a[0:7]
print (aSubstring)

#String are immutable. You can't change an existing one, just create a new
#one by doing some tricks

print ("")
print ("Replacing T for R")
b="R" + aSubstring[1:]
print (b)

print ("")
print ("Checking for existence of a Z in the string...")
zExists='z' in b
print (zExists)

print ("")
print ("Remove the vowels from the string: Laura la sin par de Caurimare")
vowels="AEIOUaeiou"
sentence="Laura la sin par de Caurimare"
newsentence=""
for letter in sentence:
    if not letter in vowels:
        newsentence=newsentence + letter
print (newsentence)

print ("")
print ("Listing the individual words in the the string:  Laura la sin par de Caurimare")
wordList = sentence.split()
for word in wordList:
    print (word)

print("")
print ("Let\'s replace dynamic parameters in a string")
print ("This is the oringinal string")
mensaje="Estimado {0}. La presente es para informale que su deuda actual es {1}"
print (mensaje)
name="Víctor Liendo"
deuda=3.1415926
mensaje2=mensaje.format(name,deuda)
print ("This is the new string")
print (mensaje2)

#by the way, the format function can be placed inside the print functions because
#this function parameter IS A STRING !!!

print ("PI with only two decimal places is : {0:.2f}".format(3.1415926))
