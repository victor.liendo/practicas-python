#
# This is an example to deal with LISTS handling.
# We create kind of objects from user data entries, and add those
# objects to the list using the append method...

mountains=[]
mountain=input("Enter the name of the mountain: ")
continent=input("Which continent is it located : ")
altitude=int(input("How high is the mountain, in metres [or 0 to finish] : "))
while altitude > 0:
    myMountain=(mountain,continent,altitude)
    mountains.append(myMountain)
    mountain=input("Enter the name of the mountain: ")
    continent=input("Which continent is it located : ")
    altitude=int(input("How high is the mountain, in metres [or 0 to finish] : "))
print("")
for (n,c,a) in mountains:
    print (n,end="\t\t\t")
    print (c,end="\t\t\t")
    print (a)


#
# Here we use list to construct MATRIXES:
#

mymatrix=[[("Algebra" , 20) , ("Maths" , 18), ("Algortihms" , 17)] , [("Algebra" , 15) , ("Maths" , 13) , ("Algortihms" , 19)] , [("Algebra" , 17) , ("Maths" , 16) , ("Algortihms" , 16)]]
print ("Here you have my scores by semester:")

for i in range(len(mymatrix)):
    print (str(i+1)+"-Semester")
    for j in range (len(mymatrix[i])):
        (subject,score)=mymatrix[i][j]
        print ("\t" + subject + "\t" + str(score))


