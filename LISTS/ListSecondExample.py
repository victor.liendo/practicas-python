#
# This is another example to deal with LISTS handling.
# In this case, we create or initialize a lists of students, each element on the list
# also having a list of the subjects students have enrolled in the current semester

students = [
    ("Samuel Liendo","Computer Science",["Discrete Maths", "Physics","Algorithms","Informatics"]),
    ("Paula Liendo","Arts",["Maths", "Latin", "French","History","English"]),
    ("Andrés Aiffil","Engineering",["Maths", "Latin", "French","History","English"])
]

#APPENDING AT THE END  OF THE LIST
students.append(("Víctor Liendo","Computer Science",["Algorithms II","Software Engineering","Maths"]))
#INSERTING AT THE BEGINNING ...
students.insert(0,("Natalí MUJICA HEREDIA","Bussines Admin",["Accounting I","Finantial Intro"]))
#REVERSING THE LIST
students.reverse();

students2=[("Yolima Carvajal", "Computer Science",["Discrete Maths", "Physics","Algorithms","Informatics"])]

students=students + students2 # el operardor + permite concatenar listas

mathcounter=0
isSomeoneStudyingChemistry=False;

for name, career, subjects in students:  ## CON OBTENGO CADA ELEMENTO DE LA LISTA
    print ("Student name: {0} \tCareer: {1}".format(name,career))

    for (i,v) in enumerate(subjects):    ## CON ESTE FOR OBTENGO CADA ELEMENTO DE LA LISTA
                                         ## COMO UN PAR (indice,valor)
        print ("\t"+ str(i) + "\t" + v)
        if v == "Maths":
            mathcounter+=1
    if "Chemistry" in subjects:
        isSomeoneStudyingChemistry=True


print ("Math has " + str(mathcounter) + " students enrolled " )
if not isSomeoneStudyingChemistry:
    print("Nobody is currently studying CHEMISTRY ...")


##
## Some other methods: sort,remove ...
##
#UN PATRON PARA CREAR Y RETORNR UNA LISTA

## def ...
## initialize the list (for example: weekdays=[]
## loop
##   create a new element
##   append it to the list
## return the list

