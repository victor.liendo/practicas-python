#
# En este ejemplo, cargamos un vocabulario de un archivo de texto, así como
# también el 1er capítulo de un libro. LA IDEA ES CONOCER cuales palabras
# del libro no están en el vocabulario.

import sys
import StringModule as sm # My string module (StringModule.py)

def test(actual, expected):
    """ Compare the actual to the expected value,
        and print a suitable message.
    """
    linenum = sys._getframe(1).f_lineno   # Get the caller's line number.
    if (expected == actual):
        msg = "Test on line {0} passed.".format(linenum)
    else:
        msg = ("Test on line {0} failed. Expected '{1}', but got '{2}'."
                .format(linenum, expected, actual))
    print(msg)


def search_linear(list, element):
    """ Find and return the index of the element [element] in LIST  [list] """
    for (i, v) in enumerate(list):  # Esta linea forma, para cada elementode la lista
       if v == element:             # xs, un par (i,v), siendo i  la pos
           return i                 # del elemento en la lista, y v su valor
    return -1

def find_unknown_words(list1, list2):
   unknown_words=[]  #Lista vacía
   for word in list2:
       if search_linear(list1, word) < 0:
           unknown_words.append(word)
   return unknown_words



def load_vocabulary_from_file(filename):
    """ Read words from filename, return list of words. """
    f = open(filename, "r")
    file_content = f.read()      # file_content será un STRING con todas las palabras del archivo
    f.close()
    vocabulary = file_content.split()   # wocabulary es una LISTA con todas las palabras del archivo
    return vocabulary

def load_book_from_file(filename):
    """ Read words from filename, return CLEAN list of words. """
    f = open(filename, "r")
    file_content = f.read()      # file_content será un STRING con todas las palabras del archivo
    f.close()
    cleaned_text = sm.text_to_words(file_content)
    return cleaned_text


# test(sm.text_to_words("My name is Earl!"), ["my", "name", "is", "earl"])

vocabulary=load_vocabulary_from_file("vocab.txt")
print("HAY {0} PALABRAS EN EL VOCABULARIO, las primeras 4 son :  {1} ".format(len(vocabulary), vocabulary[:4]))

# test(search_linear(vocabulary, "Claudio"), 0)

book=load_book_from_file("alice_in_wonderland.txt")
print("HAY {0} PALABRAS EN EL LIBRO, las primeras 4 son :  {1} ".format(len(book), book[:4]))


#Encontremos ahora las palabras del liubro que no están en el vocabulario

unknown_words=find_unknown_words(vocabulary,book)
print("HAY {0} PALABRAS del LIBRO que no están en el vocabulario".format(len(unknown_words)))
