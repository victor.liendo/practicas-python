import sys

def test(actual, expected):
    """ Compare the actual to the expected value,
        and print a suitable message.
    """
    linenum = sys._getframe(1).f_lineno   # Get the caller's line number.
    if (expected == actual):
        msg = "Test on line {0} passed.".format(linenum)
    else:
        msg = ("Test on line {0} failed. Expected '{1}', but got '{2}'."
                .format(linenum, expected, actual))
    print(msg)

def search_linear(list, element):
    """ Find and return the index of the element [element] in LIST  [list] """
    for (i, v) in enumerate(list):  # Esta linea forma, para cada elementode la lista
       if v == element:            # xs, un par (i,v), siendo i  la pos
           return i               # del elemento en la lista, y v su valor
    return -1


friends = ["Claudio", "Luis Miguel", "Yarisma", "Adolfredo", "Cassiel"]
test(search_linear(friends, "Claudio"), 0)
test(search_linear(friends, "Luis Miguel"), 1)
test(search_linear(friends, "Cassiel"), 4)
test(search_linear(friends, "Julieta"), 2)
test(search_linear(friends, "Julio"), -1)