##
## MyStringModule: A bunch of string operations
##
#1) REPLACING a substr in a whole string ...
#
# usage: newstring=strreplace(oldsubstring, newsubstring, string)
# where:
# oldsubstring = what you want to replace
# newsubstring = the replacing substring
# string = the original string
#
def strreplace(old, new, s):
    return new.join(s.split(old))


#2) CLEANING A TEXT (removing symbols and punctuation chars, and lowercasing
# the text). Return a word LIST

def text_to_words(the_text):
    my_substitutions = the_text.maketrans(
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!\"#$%&()*+,-./:;<=>?@[]^_`{|}~'\\",
      "abcdefghijklmnopqrstuvwxyz                                          ")
    # Translate the text now.
    cleaned_text = the_text.translate(my_substitutions)
    wds = cleaned_text.split()
    return wds

