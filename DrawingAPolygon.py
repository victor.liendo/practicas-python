#
# Using the TURTLE MODULE to draw a POLYGON
# The program must check that
# 1) number of sides >= 3
# 2) 360 % (number of sides) = 0, for the turtle to draw a closed POLYGON
# This way, the turtle can draw
#    a triangle
#    a square
#    a heptagon
#    a octagon
#    a nonagon
#    a decagon
#    and some more geometric figures
#
import turtle
#
ttlbgcolor=input("What color do you like background to be ?: ")
ttlcolor=input("What color do you like your LINE to be drawn ?: ")
ttlpensize=int(input("What size do you want for your LINE  border ?: "))
number_of_sides=int(input("How many sides is your POLYGON going to be ?: "))

if number_of_sides < 3:
    print ("CLOSED POLYGON can't be drawn. It must be 3 sides or more ...")
else:
    if (360 % number_of_sides) != 0:
        print ("CLOSED POLYGON can't be drawn. 360 must be exactly DIVIDED by the number of sides ...")
    else:
        ttllong=int(input("How long is the POLYGON base going to be ?: "))
        if ttllong < 5:
            print ("POLYGON base is too short. Choose a number greater than 4 ...")
        else:
            wn = turtle.Screen()
            wn.bgcolor(ttlbgcolor)          # Set the window background color
            wn.title("Hello, Tess! Please draw a CLOSED POLYGON")        # Set the window title
            tess = turtle.Turtle()
            tess.color(ttlcolor)            # Tell tess to change her color
            tess.pensize(ttlpensize)        # Tell tess to set her pen width
            #
            # the turtle head will be turned "turn_angle" angles "number_of_sides" times
            # for example, if number of sides is 6 the turtle head will be turned 60 degrees
            # to its left a total of 6 times ...
            #
            turn_angle = int(360 / number_of_sides)
            for i in range(number_of_sides):
                tess.forward(ttllong)
                tess.left(turn_angle)
            wn.mainloop()
