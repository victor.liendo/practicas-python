#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
def mostrar_opciones(tamano_buzon):
    if tamano_buzon > 0:
        print ("0.- Mostrar el BUZON de MENSAJES")
        print ("1.- Agregar mensaje al BUZON")
        print ("2.- LEER MENSAJE")
        print ("3.- ELIMINAR MENSAJE")
        print ("4.- Vaciar BUZON de MENSAJES")
        print ("5.- SALIR del SISTEMA")
    else:
        print ("1.- Agregar mensaje al BUZON")
        print ("5.- SALIR del SISTEMA")


class Mensaje:
    """ Esta clase representa un mensaje de texto """

    def __init__(self, mensaje):
        """ Crea un nuevo mensaje. El mensaje será una tupla"""
        """ (origen, fecha, asunto, cuerpo, estatus)"""
        self.mensaje=mensaje

    def obtener_contenido(self):
        return self.mensaje

    def actualizar_contenido(self,mensaje):
        self.mensaje=mensaje

    def __str__(self):
        return ("{0}".format(self.mensaje))


class BuzonMensajes:
    """ Esta clase implementa el Buzon de mensajes"""
    """ El buzón sera una lista"""
    def __init__(self):
        self.buzon=[]

    def agregar_mensaje(self, mensaje):
        self.buzon.append(mensaje)

    def eliminar_mensaje(self,pos):
        if pos >= 0 and pos < len(self.buzon):
            del(self.buzon[pos])

    def limpiar_buzon(self):
        self.buzon=[]

    def obtener_tamano(self):
        return len(self.buzon)

    def obtener_mensajes_no_leidos(self):
        count=0
        for mensaje in self.buzon:
            contenido=mensaje.obtener_contenido()
            (origen,fecha,asunto,cuerpo,estatus)=contenido
            if estatus=="N":
                count+=1
        return count

    def mostrar_buzon(self):
        """Cada elemento del buzon es un objeto Mensaje"""
        """Por ello hay que obtener del objeto el atributo mensaje"""
        """para poder acceder al contenido del mismo"""
        i=0
        count=0
        for mensaje in self.buzon:
            contenido=mensaje.obtener_contenido()
            (origen,fecha,asunto,cuerpo,estatus)=contenido
            if estatus=="N":
                count+=1
            """Invocar print sobre el objeto Mensaje invoca el método"""
            """__str__  del objeto Mensaje, y el valor retornado es impreso"""
            #print(mensaje)
            print ("{0}\t\{1}\t{2}\t{3}\t{4}".format(i,origen,fecha,estatus,asunto))
            i+=1
        print ("")
        print ("Cantidad de Mensajes:{0}\tNo leídos:{1}".format(self.obtener_tamano(),count))
        print ("")

    def mostrar_mensaje(self,i):
        mensaje=self.buzon[i]
        contenido=mensaje.obtener_contenido()
        (origen,fecha,asunto,cuerpo,estatus)=contenido
        print ("De : {0}".format(origen))
        print ("Fecha : {0}".format(fecha))
        print ("Asunto : {0}".format(asunto))
        print ("")
        print ("\t {0}".format(cuerpo))
        """Después de mostrar el mensaje, actualizamos su estatus"""
        """y actualizamos el buzón"""
        estatus=""
        contenido=(origen,fecha,asunto,cuerpo,estatus)
        mensaje.actualizar_contenido(contenido)
        self.buzon[i]=mensaje

    def __str__(self):
        return ("{0}".format(self.buzon))


b=BuzonMensajes()
#m=Mensaje(("vliendo@cnti.gob.ve","2017-04-14 10:00","Mensaje de Prueba","Este es un mensaje de prueba...","N"))
#b.agregar_mensaje(m)
print ("")
opcion=0
while opcion != 5:
    """Método obtener_tamano"""
    tamano=b.obtener_tamano()
    mostrar_opciones(tamano)
    print ("")
    opcion=int(input("Introduzca una OPCIÓN :"))
    if opcion < 5:
        if opcion==0:
            print ("")
            if tamano == 0:
                print("\t OPCION INVALIDA. Buzon está vacío")
            else:
                """Método mostrar_buzon"""
                b.mostrar_buzon()
            print ("")
        elif opcion==1:
            print ("")
            mensaje=input("Introduzca REMITENTE, FECHA, ASUNTO y CONTENIDO [separados por /] :")
            partesmensaje=mensaje.split("/")
            if len(partesmensaje) != 4:
                print ("\t Mensaje MAL FORMADO. Intente de NUEVO...")
            else:
                """Metodos crear mensaje y agregar_mensaje"""
                m=Mensaje((partesmensaje[0],partesmensaje[1],partesmensaje[2],partesmensaje[3],"N"))
                b.agregar_mensaje(m)
                print("\t Mensaje AGREGADO AL BUZON")
                print("")
                b.mostrar_buzon()
            print ("")
        elif opcion==2:
            """Método mostrar_mensaje"""
            print ("")
            if tamano == 0:
                print("\t OPCION INVALIDA. Buzón está vacio")
            else:
                i=int(input("Introduzca NRO DEL MENSAJE DESEADO [0 <= NRO < "+ str(b.obtener_tamano()) + "] :"))
                if i < 0 or i > tamano - 1:
                    print ("\t Mensaje NO EXISTE. Intente de NUEVO...")
                else:
                    b.mostrar_mensaje(i)
            print("")
        elif opcion==3:
            """Método eliminar_mensaje"""
            print ("")
            if tamano == 0:
                print("\t OPCION INVALIDA. Buzón está vacio")
            else:
                i=int(input("Introduzca NRO DEL MENSAJE A ELIMINAR [0 <= NRO < "+ str(b.obtener_tamano()) + "] :"))
                if i < 0 or i > tamano - 1:
                    print ("\t Mensaje NO EXISTE. Intente de NUEVO...")
                else:
                    b.eliminar_mensaje(i)
                    print("\t Mensaje ELIMINADO !!!")
                    print("")
                    b.mostrar_buzon()
            print("")
        elif opcion==4:
            """método limpiar_buzon"""
            print ("")
            if tamano == 0:
                print("\t OPCION INVALIDA. El buzón ya está vacío")
            else:
                b.limpiar_buzon()
                print("\t TODOS LOS MENSAJES HAN SIDO ELIMINADOS")
            print("")
        else:
            continue









#m=Mensaje(("vliendo@cnti.gob.ve","2017-04-14 10:00","Mensaje de Prueba","Este es un mensaje de prueba...","N"))
#b.agregar_mensaje(m)
#m=Mensaje(("victor.liendo@gmail.com","2017-04-14 10:00","Tareas Pendientes","Cargar el código arancelario en la BD","N"))
#b.agregar_mensaje(m)
#b.mostrar_buzon()
#print("")
#print("Cantidad de mensajes no leidos: {0}".format(b.obtener_mensajes_no_leidos()))
#print("")
#b.mostrar_mensaje(0)
#print("")
#b.eliminar_mensaje(0)
#b.mostrar_buzon()
#print("")
#print("Cantidad de mensajes no leidos: {0}".format(b.obtener_mensajes_no_leidos()))
#print("")
#b.mostrar_mensaje(1)
#print("")
#b.mostrar_buzon()
#print("")
#print("Cantidad de mensajes no leidos: {0}".format(b.obtener_mensajes_no_leidos()))
#b.limpiar_buzon()
#print("")
#print("Tamaño del buzón despues de vaciar: {0}".format(b.obtener_tamano()))
