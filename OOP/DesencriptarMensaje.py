#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

#Una  manera  de  encriptar   mensajes(no  muy  segura),   es  colocar   corchetes   de   manera
#arbitraria, y todo lo que está entre corchetes ponerlo al revés, por ejemplo “Olimpíada de
#Informática” se puede encriptar como “Olimpía[ad] de I[rofn]mática”, los corchetes también se pueden
#anidar, es decir, otra forma de encriptar el mismo mensaje podría ser “Olimpía[ám[nfor]I ed [da]]tica”.
#Escribe un programa, que dado un mensaje encriptado, determine el mensaje original en tiempo lineal.
#(9° Olimpiada Mexicana de Informática)

import sys
class Mensaje:
    """ Esta clase representa un mensaje de texto """

    def __init__(self, mensaje):
        """ Crea un nuevo mensaje. El mensaje será una secuencia """
        """de caracteres, posiblemente encriptadoo, usando CORCHETES"""
        self.mensaje=mensaje
    def obtener_longitud(self):
        return len(self.mensaje)

    def obtener_pos_cierre(self,init_pos):
        i=init_pos + 1
        end_pos=-1
        balance=1
        while i < self.obtener_longitud() and balance != 0:
            if self.mensaje[i]=="[":
                balance+=1
            else:
                if self.mensaje[i]=="]":
                    balance-=1
            if balance == 0:
                end_pos=i
            i+=1
        return end_pos

    def obtener_pos_inicio(self,end_pos):
        i=end_pos - 1
        init_pos=-1
        balance=1
        while i >= 0 and balance != 0:
            if self.mensaje[i]=="[":
                balance-=1
            else:
                if self.mensaje[i]=="]":
                    balance+=1
            if balance == 0:
                init_pos=i
            i-=1
        return init_pos

    def desencriptar(self,inicio,fin):    #Ejemplo : Olimpía[ám[nfor]I ed [da]]tica
        #print(str(inicio) + "-" + str(fin))
        desencriptado=""
        i=inicio
        if i < fin:
            """Recorriendo el mensaje de izquierda a derecha"""
            """El mensaje retornado es el mismo hasta que conseguimos"""
            """un [. En ese momento, buscamos el ] que lo cierra y"""
            """mandamos a desencriptar entre esas dos posiciones """
            """(corchete que cierra y abre, en sentido reverso fin < inicio"""
            while i < fin:
                if self.mensaje[i] != "[":
                    if self.mensaje[i] == "]":
                        desencriptado="Mensaje MAL FORMADO"
                        i=fin
                    else:
                        desencriptado=desencriptado+self.mensaje[i]
                else:
                    poscierre=self.obtener_pos_cierre(i)
                    if poscierre <  0:
                        desencriptado="Mensaje MAL FORMADO"
                        i=fin
                    else:
                        desencriptado=desencriptado + self.desencriptar(poscierre-1,i)
                        i=poscierre
                i+=1
        else:
            """Recorriendo el mensaje de derecha a izquierda"""
            """El mensaje retornado es la cadena invertida hasta que conseguimos"""
            """un ]. En ese momento, buscamos el [ que lo abre y"""
            """mandamos a desencriptar entre esas dos posiciones """
            """(corchete que abre y cierra, en sentido normal inicio < fin"""
            while i > fin:
                if self.mensaje[i] != "]":
                    if self.mensaje[i]=="[":
                        desencriptado="Mensaje MAL FORMADO"
                        i=fin
                    else:
                        desencriptado=desencriptado+self.mensaje[i]
                else:
                    posinicio=self.obtener_pos_inicio(i)
                    if posinicio < 0:
                        desencriptado="Mensaje MAL FORMADO"
                        i=fin
                    else:
                       desencriptado=desencriptado + self.desencriptar(posinicio+1,i)
                       i=posinicio
                i-=1
        return desencriptado


def test(actual, expected):
    """ Compare the actual to the expected value,
        and print a suitable message.
    """
    linenum = sys._getframe(1).f_lineno   # Get the caller's line number.
    if (expected == actual):
        msg = "Test on line {0} passed.".format(linenum)
    else:
        msg = ("Test on line {0} failed. Expected '{1}', but got '{2}'."
                .format(linenum, expected, actual))
    print(msg)


#test(mensaje.obtener_pos_cierre(21),24)
#test(mensaje.obtener_pos_inicio(25),21)
#mensaje=Mensaje("Olimpía[ám[nfor]I al ed [da]]tica")
#test(mensaje.obtener_pos_cierre(21),24)
#test(mensaje.obtener_pos_inicio(25),21)
mensaje=Mensaje("[[[]]]")
test(mensaje.desencriptar(0,mensaje.obtener_longitud()),"")
mensaje=Mensaje("Olimpíada [al ed] Informática")
test(mensaje.desencriptar(0,mensaje.obtener_longitud()),"Olimpíada de la Informática")
mensaje=Mensaje("Ma[anañ] e[le s] d[aí]")
test(mensaje.desencriptar(0,mensaje.obtener_longitud()),"Olimpíada de la Informática")
mensaje=Mensaje("Ma[anañ] ele s] d[aí]")
test(mensaje.desencriptar(0,mensaje.obtener_longitud()),"Olimpíada de la Informática")