#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This is a way to implement a Card object



class Card:
    """suits y ranks son como variables globales de la clase.."""
    """el valorf narf es usado para almacenar algo en la posición"""
    """0, que para el jugo no tiene sentido, ya que no carta numerada con 0'"""

    suits = ["Clubs", "Diamonds", "Hearts", "Spades"]
    ranks = ["narf", "Ace", "2", "3", "4", "5", "6", "7","8", "9", "10", "Jack", "Queen", "King"]
    ncards = 0

    def cards_played(self):
        return self.ncards

    def __init__(self, suit=0, rank=0):
        """The Card  object is implemented with two Integers, the suits and ranks indexes ..."""
        self.suit = suit
        self.rank = rank
        self.__private_var="Soy CUASI-Privada. porque puedo ser accedida directamente como _Card__private_var"
        #print(self.ncards)
        #self.__dict__={}

    def return_private_var(self):
        return self.__private_var

    def __str__(self):
        return (self.ranks[self.rank] + " of " + self.suits[self.suit])

    def compare(self, other):
    # Check the suits
        if self.suit > other.suit:
            return 1
        elif self.suit < other.suit:
            return -1
        else:
    # Suits are the same... check ranks
            if self.rank > other.rank:
                return 1
            elif self.rank < other.rank:
                return -1
            else:
    # Ranks are the same... CARDS ARE EQUALS
                return 0
    def __eq__(self, other):
        return self.compare(other) == 0

    def __le__(self, other):
        return self.compare(other) <= 0

    def __ge__(self, other):
        return self.compare(other) >= 0

    def __gt__(self, other):
        return self.compare(other) > 0

    def __lt__(self, other):
        return self.compare(other) < 0

    def __ne__(self, other):
        return self.compare(other) != 0


class Pack:
    """Implementa un paquete de cartas"""
    def __init__(self):
        self.cards = []
        for suit in range(4):
            for rank in range(1, 14):
                """Creo un objeto de tipo Card y lo agrego al objeto Pack"""
                self.cards.append(Card(suit, rank))

    def __str__(self):
        str=""
        for card in self.cards:
            str+=(card.__str__() +"\n")
        return str



card=Card(1,4)
card.ncards+=1
"""print(card) invokes the __str__ method of Card object"""
print("My card is a/an {0}".format(card))
card2=Card(3,2)
card.ncards+=1
print("The other card is a/an {0}".format(card2))
print("")
print ("Which Card is greater ?")
print("")
"""print(card) invokes the __str__ method of Card object"""
comparison=card.compare(card2)
if comparison < 0:
    print(card, end=" ")
    print("IS SMALLER THAN",end=" ")
    print(card2)
elif comparison > 0:
    print(card, end=" ")
    print("IS BIGGER THAN",end=" ")
    print(card2)
else:
    print(card, end=" ")
    print("IS THE SAME CARD AS",end=" ")
    print(card2)

""" operators       ==,   <,   >,   <=,   >=,   != """
""""will invoque    eq    lt   gt   le    ge    ne """

if card < card2:
    print(card, end=" ")
    print("IS SMALLER THAN",end=" ")
    print(card2)
print("")
pack=Pack()
print(pack)
print("")
"""ncards PARECE SER ACCESIBLE DENTRO DE LA CLASE, no desde fuera de ella"""
"""So the below printing operations  don't work ...'"""
print("Number or cards played so far: ".format(card.ncards))
print("Number or cards played so far: ".format(card.cards_played()))
#
#
# By the way
# 1) no need to declare class members, they come to existence once
# they apper in the constructor
#
# 2) The way the members are named, i.e. suit and rank, they are PUBLIC
print ("La CARTA 1 es un {0}/{1}".format(card.suit,card.rank))
"""La siguiente instrucción me da acceso a toda la definición del objeto Card"""
"""lo cual no permite el encapsulamiento"""
print (card.__dict__)
"""La siguiente instrucción es como un getter ..."""
print("El valor de mi atributo cuasi-privado [porque no lo es realmente] es: {0}".format(card.return_private_var()))
