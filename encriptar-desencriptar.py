#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
caractercadena=""
T=4
U=20
cadenabinaria=""
cadenaencriptada=""
operacion=input ("Introduzca la operación a realizar : ")
print "Usted introdujo :" + operacion
if operacion <> "encriptar" and operacion <> "desencriptar" :
	print "ERROR: Operacion DESCONOCIDA"
else:
	#LA OPERACION ES VALIDA
	if operacion == "encriptar":
		print ""
		cadena=input ("Introduzca la cadena de caracteres a encriptar : ")
		print "Usted introdujo: " + cadena
		print ""
		if len(cadena) > T:
			#LA CADENA ES MAYOR QUE EL LIMITE ESTABLECIDO
			print "ERROR: LONGITUD DE CADENA MAYOR QUE EL LIMITE : " + str(T)
		else:
			#AHORA VAMOS A VERIFICAR QUE LA CADENA CONTIENE SOLO CARACTERES VALIDOS
			if cadena.isalpha() is False:
				print "ERROR: CADENA CONTIENE CARACTERES INVÁLIDOS"
			else:
				if len(cadena) < T:
					cadena=cadena.rjust(T)
				#procesar primera letra de la cadena
				caractercadena=cadena[0]
				if  caractercadena == "a":
					caractercadena="1"
				elif caractercadena == "b":
					caractercadena="2"
				elif caractercadena == "c":
					caractercadena="3"
				elif caractercadena == "d":
					caractercadena="4"
				elif caractercadena == "e":
					caractercadena="5"
				elif caractercadena == "f":
					caractercadena="6"
				elif caractercadena == "g":
					caractercadena="7"
				elif caractercadena == "h":
					caractercadena="8"
				elif caractercadena == "i":
					caractercadena="9"
				elif caractercadena == "j":
					caractercadena="10"
				elif caractercadena == "k":
					caractercadena="11"
				elif caractercadena == "l":
					caractercadena="12"
				elif caractercadena == "m":
					caractercadena="13"
				elif caractercadena == "n":
					caractercadena="14"
				elif caractercadena == "o":
					caractercadena="15"
				elif caractercadena == "p":
					caractercadena="16"
				elif caractercadena == "q":
					caractercadena="17"
				elif caractercadena == "r":
					caractercadena="18"
				elif caractercadena == "s":
					caractercadena="19"
				elif caractercadena == "t":
					caractercadena="20"
				elif caractercadena == "u":
					caractercadena="21"
				elif caractercadena == "v":
					caractercadena="22"
				elif caractercadena == "w":
					caractercadena="23"
				elif caractercadena == "x":
					caractercadena="24"
				elif caractercadena == "y":
					caractercadena="25"
				elif caractercadena == "z":
					caractercadena="26"
				else:
					caractercadena="0"
				#print caractercadena
				if caractercadena == "1":
					caractercadena = "00001"
				elif caractercadena == "2":
					caractercadena = "00010"
				elif caractercadena == "3":
					caractercadena = "00011"
				elif caractercadena == "4":
					caractercadena = "00100"
				elif caractercadena == "5":
					caractercadena = "00101"
				elif caractercadena == "6":
					caractercadena = "00110"
				elif caractercadena == "7":
					caractercadena = "00111"
				elif caractercadena == "8":
					caractercadena = "01000"
				elif caractercadena == "9":
					caractercadena = "01001"
				elif caractercadena == "10":
					caractercadena = "01010"
				elif caractercadena == "11":
					caractercadena = "01011"
				elif caractercadena == "12":
					caractercadena = "01100"
				elif caractercadena == "13":
					caractercadena = "01101"
				elif caractercadena == "14":
					caractercadena = "01110"
				elif caractercadena == "15":
					caractercadena = "01111"
				elif caractercadena == "16":
					caractercadena = "10000"
				elif caractercadena == "17":
					caractercadena = "10001"
				elif caractercadena == "18":
					caractercadena = "10010"
				elif caractercadena == "19":
					caractercadena = "10011"
				elif caractercadena == "20":
					caractercadena = "10100"
				elif caractercadena == "21":
					caractercadena = "10101"
				elif caractercadena == "22":
					caractercadena = "10110"
				elif caractercadena == "23":
					caractercadena = "10111"
				elif caractercadena == "24":
					caractercadena = "11000"
				elif caractercadena == "25":
					caractercadena = "11001"
				elif caractercadena == "26":
					caractercadena = "11010"
				else:
					caractercadena = "00000"
				#print caractercaden
				cadenabinaria = cadenabinaria + caractercadena
				#procesar segunda letra de la cadena
				caractercadena=cadena[1]
				if  caractercadena == "a":
					caractercadena="1"
				elif caractercadena == "b":
					caractercadena="2"
				elif caractercadena == "c":
					caractercadena="3"
				elif caractercadena == "d":
					caractercadena="4"
				elif caractercadena == "e":
					caractercadena="5"
				elif caractercadena == "f":
					caractercadena="6"
				elif caractercadena == "g":
					caractercadena="7"
				elif caractercadena == "h":
					caractercadena="8"
				elif caractercadena == "i":
					caractercadena="9"
				elif caractercadena == "j":
					caractercadena="10"
				elif caractercadena == "k":
					caractercadena="11"
				elif caractercadena == "l":
					caractercadena="12"
				elif caractercadena == "m":
					caractercadena="13"
				elif caractercadena == "n":
					caractercadena="14"
				elif caractercadena == "o":
					caractercadena="15"
				elif caractercadena == "p":
					caractercadena="16"
				elif caractercadena == "q":
					caractercadena="17"
				elif caractercadena == "r":
					caractercadena="18"
				elif caractercadena == "s":
					caractercadena="19"
				elif caractercadena == "t":
					caractercadena="20"
				elif caractercadena == "u":
					caractercadena="21"
				elif caractercadena == "v":
					caractercadena="22"
				elif caractercadena == "w":
					caractercadena="23"
				elif caractercadena == "x":
					caractercadena="24"
				elif caractercadena == "y":
					caractercadena="25"
				elif caractercadena == "z":
					caractercadena="26"
				else:
					caractercadena="0"
				#print caractercadena
				if caractercadena == "1":
					caractercadena = "00001"
				elif caractercadena == "2":
					caractercadena = "00010"
				elif caractercadena == "3":
					caractercadena = "00011"
				elif caractercadena == "4":
					caractercadena = "00100"
				elif caractercadena == "5":
					caractercadena = "00101"
				elif caractercadena == "6":
					caractercadena = "00110"
				elif caractercadena == "7":
					caractercadena = "00111"
				elif caractercadena == "8":
					caractercadena = "01000"
				elif caractercadena == "9":
					caractercadena = "01001"
				elif caractercadena == "10":
					caractercadena = "01010"
				elif caractercadena == "11":
					caractercadena = "01011"
				elif caractercadena == "12":
					caractercadena = "01100"
				elif caractercadena == "13":
					caractercadena = "01101"
				elif caractercadena == "14":
					caractercadena = "01110"
				elif caractercadena == "15":
					caractercadena = "01111"
				elif caractercadena == "16":
					caractercadena = "10000"
				elif caractercadena == "17":
					caractercadena = "10001"
				elif caractercadena == "18":
					caractercadena = "10010"
				elif caractercadena == "19":
					caractercadena = "10011"
				elif caractercadena == "20":
					caractercadena = "10100"
				elif caractercadena == "21":
					caractercadena = "10101"
				elif caractercadena == "22":
					caractercadena = "10110"
				elif caractercadena == "23":
					caractercadena = "10111"
				elif caractercadena == "24":
					caractercadena = "11000"
				elif caractercadena == "25":
					caractercadena = "11001"
				elif caractercadena == "26":
					caractercadena = "11010"
				else:
					caractercadena = "00000"
				#print caractercadena
				cadenabinaria = cadenabinaria + caractercadena
				#procesar tercera letra de la cadena
				caractercadena=cadena[2]
				if  caractercadena == "a":
					caractercadena="1"
				elif caractercadena == "b":
					caractercadena="2"
				elif caractercadena == "c":
					caractercadena="3"
				elif caractercadena == "d":
					caractercadena="4"
				elif caractercadena == "e":
					caractercadena="5"
				elif caractercadena == "f":
					caractercadena="6"
				elif caractercadena == "g":
					caractercadena="7"
				elif caractercadena == "h":
					caractercadena="8"
				elif caractercadena == "i":
					caractercadena="9"
				elif caractercadena == "j":
					caractercadena="10"
				elif caractercadena == "k":
					caractercadena="11"
				elif caractercadena == "l":
					caractercadena="12"
				elif caractercadena == "m":
					caractercadena="13"
				elif caractercadena == "n":
					caractercadena="14"
				elif caractercadena == "o":
					caractercadena="15"
				elif caractercadena == "p":
					caractercadena="16"
				elif caractercadena == "q":
					caractercadena="17"
				elif caractercadena == "r":
					caractercadena="18"
				elif caractercadena == "s":
					caractercadena="19"
				elif caractercadena == "t":
					caractercadena="20"
				elif caractercadena == "u":
					caractercadena="21"
				elif caractercadena == "v":
					caractercadena="22"
				elif caractercadena == "w":
					caractercadena="23"
				elif caractercadena == "x":
					caractercadena="24"
				elif caractercadena == "y":
					caractercadena="25"
				elif caractercadena == "z":
					caractercadena="26"
				else:
					caractercadena="0"
				#print caractercadena
				if caractercadena == "1":
					caractercadena = "00001"
				elif caractercadena == "2":
					caractercadena = "00010"
				elif caractercadena == "3":
					caractercadena = "00011"
				elif caractercadena == "4":
					caractercadena = "00100"
				elif caractercadena == "5":
					caractercadena = "00101"
				elif caractercadena == "6":
					caractercadena = "00110"
				elif caractercadena == "7":
					caractercadena = "00111"
				elif caractercadena == "8":
					caractercadena = "01000"
				elif caractercadena == "9":
					caractercadena = "01001"
				elif caractercadena == "10":
					caractercadena = "01010"
				elif caractercadena == "11":
					caractercadena = "01011"
				elif caractercadena == "12":
					caractercadena = "01100"
				elif caractercadena == "13":
					caractercadena = "01101"
				elif caractercadena == "14":
					caractercadena = "01110"
				elif caractercadena == "15":
					caractercadena = "01111"
				elif caractercadena == "16":
					caractercadena = "10000"
				elif caractercadena == "17":
					caractercadena = "10001"
				elif caractercadena == "18":
					caractercadena = "10010"
				elif caractercadena == "19":
					caractercadena = "10011"
				elif caractercadena == "20":
					caractercadena = "10100"
				elif caractercadena == "21":
					caractercadena = "10101"
				elif caractercadena == "22":
					caractercadena = "10110"
				elif caractercadena == "23":
					caractercadena = "10111"
				elif caractercadena == "24":
					caractercadena = "11000"
				elif caractercadena == "25":
					caractercadena = "11001"
				elif caractercadena == "26":
					caractercadena = "11010"
				else:
					caractercadena = "00000"
				#print caractercadena
				cadenabinaria = cadenabinaria + caractercadena
				#procesar última letra de la cadena
				caractercadena=cadena[3]
				if  caractercadena == "a":
					caractercadena="1"
				elif caractercadena == "b":
					caractercadena="2"
				elif caractercadena == "c":
					caractercadena="3"
				elif caractercadena == "d":
					caractercadena="4"
				elif caractercadena == "e":
					caractercadena="5"
				elif caractercadena == "f":
					caractercadena="6"
				elif caractercadena == "g":
					caractercadena="7"
				elif caractercadena == "h":
					caractercadena="8"
				elif caractercadena == "i":
					caractercadena="9"
				elif caractercadena == "j":
					caractercadena="10"
				elif caractercadena == "k":
					caractercadena="11"
				elif caractercadena == "l":
					caractercadena="12"
				elif caractercadena == "m":
					caractercadena="13"
				elif caractercadena == "n":
					caractercadena="14"
				elif caractercadena == "o":
					caractercadena="15"
				elif caractercadena == "p":
					caractercadena="16"
				elif caractercadena == "q":
					caractercadena="17"
				elif caractercadena == "r":
					caractercadena="18"
				elif caractercadena == "s":
					caractercadena="19"
				elif caractercadena == "t":
					caractercadena="20"
				elif caractercadena == "u":
					caractercadena="21"
				elif caractercadena == "v":
					caractercadena="22"
				elif caractercadena == "w":
					caractercadena="23"
				elif caractercadena == "x":
					caractercadena="24"
				elif caractercadena == "y":
					caractercadena="25"
				elif caractercadena == "z":
					caractercadena="26"
				else:
					caractercadena="0"
				#print caractercadena
				if caractercadena == "1":
					caractercadena = "00001"
				elif caractercadena == "2":
					caractercadena = "00010"
				elif caractercadena == "3":
					caractercadena = "00011"
				elif caractercadena == "4":
					caractercadena = "00100"
				elif caractercadena == "5":
					caractercadena = "00101"
				elif caractercadena == "6":
					caractercadena = "00110"
				elif caractercadena == "7":
					caractercadena = "00111"
				elif caractercadena == "8":
					caractercadena = "01000"
				elif caractercadena == "9":
					caractercadena = "01001"
				elif caractercadena == "10":
					caractercadena = "01010"
				elif caractercadena == "11":
					caractercadena = "01011"
				elif caractercadena == "12":
					caractercadena = "01100"
				elif caractercadena == "13":
					caractercadena = "01101"
				elif caractercadena == "14":
					caractercadena = "01110"
				elif caractercadena == "15":
					caractercadena = "01111"
				elif caractercadena == "16":
					caractercadena = "10000"
				elif caractercadena == "17":
					caractercadena = "10001"
				elif caractercadena == "18":
					caractercadena = "10010"
				elif caractercadena == "19":
					caractercadena = "10011"
				elif caractercadena == "20":
					caractercadena = "10100"
				elif caractercadena == "21":
					caractercadena = "10101"
				elif caractercadena == "22":
					caractercadena = "10110"
				elif caractercadena == "23":
					caractercadena = "10111"
				elif caractercadena == "24":
					caractercadena = "11000"
				elif caractercadena == "25":
					caractercadena = "11001"
				elif caractercadena == "26":
					caractercadena = "11010"
				else:
					caractercadena = "00000"
				#print caractercadena
				cadenabinaria = cadenabinaria + caractercadena
				#EN ESTE MOMENTO TENEMOS TODA LA CADENA ENCRIPTADA, pero NOS FALTA INVERTIRLA
				#EJEMPLO. Si la cadena original es "abab", cadenacriptada tendrá: 00001000100000100010
				#AHORA VAMOS A GENERAR UNA CADENA CON LOS BITS INVERTIDOS
				print "La cadena binaria es:" + cadenabinaria
				if cadenabinaria[0] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[1] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[2] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[3] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[4] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[5] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[6] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[7] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[8] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[9] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[10] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[11] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[12] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[13] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[14] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[15] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[16] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[17] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[18] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				if cadenabinaria[19] == "0":
					cadenaencriptada = cadenaencriptada + "1"
			 	else:
					cadenaencriptada = cadenaencriptada + "0"
				print "la cadena encriptada es: " + cadenaencriptada
	else:
		print ""
		cadena=input ("Introduzca la cadena de caracteres a desencriptar : ")
		print "Usted introdujo: " + cadena
		if len(cadena) <> 20:
			print "Longitud de la cadena es INVÁLIDA  "
		else:
		#VERIFICAMOS QUE SÓLO HAY 0's y 1's en la cadena
			if (cadena[0] not in ("0","1")
			or  cadena[1] not in ("0","1")
			or  cadena[2] not in ("0","1")):
				print "CADENA CONTIENE CARACTERES INVÁLIDOS"
