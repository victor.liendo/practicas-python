#
# Working with files
#
#
#

#
# En este ejemplo, se lee un archivo y se le agrega un consecutivo al comienzo
# de cada registro
#
import os
import string
count=0
inputFile=open("/home/victorl/Documentos/DATAScience/LoanExample/LoanDataSet.csv","r")
outputFile=open("/home/victorl/Documentos/DATAScience/LoanExample/LoanDataSetWithID.csv","w")
outputFile.write("LoanID,Gender,Married,Dependents,Education,SelfEmployed,ApplicantIncome,CoapplicantIncome,LoanAmount,Loan_Amount_Term,CreditHistory,PropertyArea,LoanStatus\n")
line=inputFile.readline()
print("")
print("Este es el contenido del archivo")
print ("")
outputline=""


while len(line) != 0:                            # Keep reading
    count+=1
    fields=line.split(",")
    if int(fields[0]) == 0:
        fields[0]="Female"
    else:
        fields[0]="Male"

    if int(fields[1]) == 0:
        fields[1] = "N"
    else:
        fields[1] = "Y"

    if int(fields[3]) == 0:
        fields[3] = "Under Graduate"
    else:
        fields[3] = "Graduate"

    if int(fields[4]) == 0:
        fields[4] = "N"
    else:
        fields[4] = "Y"

    if int(fields[10]) == 0:
        fields[10] = "Rural"
    elif int(fields[10]) == 1:
        fields[10] = "Semi Rural"
    else:
        fields[10] = "Urban"


    if int(fields[11]) == 0:
        fields[11]="N"
    else:
        fields[11]="Y"

    outputline=",".join(fields)
    print (str(count)+","+outputline)
    #outputFile.write(str(count)+","+line)
    outputFile.write(str(count)+","+outputline+"\n")
    line=inputFile.readline()

inputFile.close()
outputFile.close()
