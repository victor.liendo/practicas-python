#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Dado un número N, el programa dibuja un triángulo de base y altura N
#
# Ejemplo: N=4
#
# ****
#  ***
#   **
#    *
#

caracter_triangulo=input("Introduzca el caracter para dibujar el triángulo: ")
tamano=int(input("Introduzca tamaño de la BASE y ALTURA del TRIÁNGULO: "))

if tamano == 0:
    print ("Tamaño de base y altura debe ser mayor que 0")
else:
    limite = tamano
    for i in range(limite):              # i es inicializado en 0 y se incrementará hasta (tamano - 1)
        espacios_izq=" " * i             # crea una secuencia de espacios en blanco de longitud "i""
        caracteres_a_imprimir = caracter_triangulo * tamano # crea una secuencia de "carater_triangulo" de longitud "tamano""
        print (espacios_izq + caracteres_a_imprimir)
        tamano-=1

