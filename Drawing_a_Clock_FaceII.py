#
# Using the TURTLE MODULE to draw a CLOCK FACE
#
#
import turtle
import math
#

turn_angle=int(360 / 12)       # 12 because a clock has 12 main positions
ttlbgcolor=input("What color do you like background to be ?: ")
ttlcolor=input("What color do you like your LINE to be drawn ?: ")
ttllong=int(input("How long are the clock ARMS going to be ?: "))

while ttllong < 50:
        ttllong=int(input("Please, the number you've entered is too short to draw a nice clock. Give me a longer one :"))



wn = turtle.Screen()
wn.bgcolor(ttlbgcolor)                         # Set the window background color
wn.title("Hello, Tess! Please draw a CLOCK...")# Set the window title
tess = turtle.Turtle()
tess.color(ttlcolor)                           # Tell tess to change her color
tess.shape("turtle")
tess.speed(1)
tess.penup()
tess.stamp()
tess.left(90)

for i in range(12):
    tess.forward(ttllong)
    tess.stamp()
    if i < 11:
        tess.backward(ttllong)
        tess.right(turn_angle)


wn.mainloop()
