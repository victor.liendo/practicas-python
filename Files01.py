#
# Working with files
#
#
# StringModule is my own module for common string operations (StringModule.py)
import StringModule as sm

#
#FIRST, let's create a new file
#'
outputFile = open("/home/victorl/tmp/mountains.txt","w")


mountain=input ("Introduzca los datos de una montaña [nombre, ubicacion, altitud], o EOF para terminar : ")
while mountain != "EOF":
    outputline=sm.strreplace(" ","|",mountain)  #sm is the variable to handle the module
    outputFile.write(outputline+"\n")
    mountain=input ("Introduzca los datos de una montaña [nombre, ubicacion, altitud], o EOF para terminar : ")
outputFile.close()

#
# Now, let's read the file'
#

inputFile=open("/home/victorl/tmp/mountains.txt","r")
mountain=inputFile.readline()
while len(mountain) != 0:                            # Keep reading forever
    print(mountain, end="")
    mountain=inputFile.readline()
inputFile.close()