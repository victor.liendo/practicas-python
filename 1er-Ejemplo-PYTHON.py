#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# The following code inspect /etc/group lines for important groups, then adds canaima user to those lines
#
import fileinput

file_name = '/home/victorl/Documents/UTILITIES/Python/group'
group_file = open(file_name, 'r')
important_groups = ["lp", "cdrom", "floppy", "audio", "dip", "video", "plugdev", "netdev", "bluetooth", "lpadmin", "powerdev", "scanner"]
important_user = "canaima"
new_lines = []

for line in group_file:
    line = line.strip() #Remove the character NewLine
    data = line.split(':')
    group = data[0]
    users = data[3]
    if group in important_groups:
        if users == '':
            new_lines.append(line + important_user)
        else:
            new_lines.append(line + ',' + important_user)
    else:
        new_lines.append(line)
group_file.close()

group_file = open(file_name, 'w')
for line in new_lines:
    print >> group_file, line

