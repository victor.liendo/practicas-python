#Contar palabras
#Cuenta las palabras en una secuencia de carcateres o string. Una palabra es
#una secuencia de 1 o mas  caracteres distintos de espacio, y está:
#antecedida por nada (comienzo del string), o "1 o mas" espacios en blanco
#seguida por uno o mas espacios en blanco
#
#un carater como "y", "o",  "u"   que cumpla las reglas anteriores, es una
#palabra.

#Las palabras pueden terminar en signos de puntuación. Si estos,
#están en cualquier posición excepto la final,hacen a la palabra inválida.
#Si la palabra contiene cualquier otro caracter no alfabético, en cualquier
#posición, la palabra es inválida

def validar_palabra (palabra):
    palabraValida=True
    if len(palabra) == 1:

                    #se encontraron símbolos, signos de
                    #de puntuación o caracteres especiales
                    #sueltos o aislados. Sólo se permiten
                    #signos de puntuación al final de una
                    #palabra (sin espacios)

                    palabraValida=False
    else:
                    #se encontraron símbolos, signos de
                    #de puntuación o caracteres especiales
                    #en alguna posición de la palabra. Sólo se permiten
                    #signos de puntuación al final de una
                    #palabra (sin espacios).De ser así, palabra es VÁLIDA

                    #1) Revisamos todas las posiciones excepto la última
                    #   Si alguna contiene caracteres no alfabéticos la
                    # palabra es inválida

                    for j in range(len(palabra)-1):
                        if palabra[j].isalpha() is False:
                            palabraValida=False
                            break

                    #Si llegamos aquí, sólo hay caracteres INVÁLIDOS al
                    #final de la palabra. Si es un signo de puntuación
                    #, exclamación o interrogación, la palabra es VÁLIDA

                    if palabraValida is True:
                        if palabra[len(palabra)-1] not in (',',':',';','.','?','!'):
                            palabraValida=False


    return palabraValida

contadorPalabras=0
palabra=""
chequearPalabra=False
secuenciaValida=True
secuencia=input("Introduzca las palabras separadas por uno o mas espacios en blanco: ")
print ("Usted introdujo:", secuencia)
for i in range(len(secuencia)):

    if secuencia[i] == " ":
        if palabra != "":
            #Si hay que chequear la palabra y es válida, se cuenta
            #Si no hay que chequearla, también se cuenta
            if chequearPalabra is True:
                if validar_palabra(palabra) is False:
                    secuenciaValida=False
                    print (palabra)
                    break

            contadorPalabras+=1
            print (palabra)
            palabra=""
            chequearPalabra=False
    else:
        palabra=palabra + secuencia[i]
        if secuencia[i].isalpha() is False:
            chequearPalabra=True


#Condicion de borde: Si el último caracter de la secuencia no es " ", la última palabra no
#fue fue tomada en cuenta

if palabra != "" :
    if chequearPalabra is True:
       if validar_palabra(palabra) is False:
            secuenciaValida=False
            print (palabra)
       else:
           contadorPalabras+=1
           print (palabra)
    else:
        contadorPalabras+=1
        print (palabra)

if secuenciaValida is True:
    print ("La cantidad de palabras encontradas es: ",contadorPalabras)
else:
    print ("Se encontraron caracteres inválidos en la secuencia ...procedimiento abortado !!!")


