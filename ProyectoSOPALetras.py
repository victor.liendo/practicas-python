#!/usr/bin/env python
# -*- coding: utf-8 -*-
#En este ejemplo, creamos un arreglo vacío de nombre “b”, de 3 filas x 2 columnas,
# de tipo string. Podemos usar 'float' o 'int'.

import numpy as arreglo
#VALIDA SI LA SECUENCIA O LINEA QUE VA EN LA FILA DE LA MATRIZ ES CORRECTA
def validar_secuencia(secuencia,M):
    if len(secuencia) != M or secuencia.isalpha() == False:
        return False
    else:
        return True

#VALIDA SI LA PALABRA A BUSCAR ES CORRECTA
def validar_palabra(palabra):
    if len(palabra) > M or palabra.isalpha() == False or len(palabra)<1:
        return False
    else:
        return True

# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos del límite de la matriz (j=M)
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_este(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and j < M and encontrada == True:
        if m[i,j] != s[k]:
            encontrada=False
        else:
            #print(str(i) + "/" + str(j))
            j+= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)

# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de la columna (j=0)
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_oeste(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and j > -1  and encontrada == True:
        if m[i,j] != s[k]:
            encontrada=False
        else:
            #print(str(i) + "/" + str(j))
            j-= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)

# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de fila i=0
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_norte(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and i > -1  and encontrada == True:
        if m[i,j] != s[k]:
            encontrada=False
        else:
            #print(str(i) + "/" + str(j))
            i-= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)

# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de fila i=N
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_sur(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and i < N  and encontrada == True:
        if m[i,j] != s[k]:
            encontrada=False
        else:
            #print(str(i) + "/" + str(j))
            i+= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)


# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de fila i=0, columna j=0
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_noroeste(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and i > -1 and j > -1  and encontrada == True:
        if m[i,j] != s[k]:
            encontrada=False
        else:
            #print(str(i) + "/" + str(j))
            i-= 1
            j-= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)


# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de fila i=0, columna j=M
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_noreste(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and i > -1 and j < M  and encontrada == True:
        if m[i,j] != s[k]:
            encontrada=False
        else:
            #print(str(i) + "/" + str(j))
            i-= 1
            j+= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)

# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de fila i=N, columna j=0
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_suroeste(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and i < N and j > -1  and encontrada == True:
        if m[i,j] != s[k]:
            encontrada=False
        else:
            #print(str(i) + "/" + str(j))
            i+= 1
            j-= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)


# 1) Tenemos que buscar desde el segundo caracter de la palabra (k=1)
# 2) Vamos a iterar mientras
#    no nos salgamos de fila i=N, columna j=N
#    no nos salgamos de la longitud de la palabra (k)
#....La comparación celda a celda entre la posición actual en la palabra y la posición actual
#    en la matriz resulte verdadera

def buscar_sureste(m, s, i, j):
    encontrada = True
    k = 1
    while k < len(s) and i < N and j < M  and encontrada == True:
        if m[i,j] != s[k]:
            encontrada=False
        else:
            #print(str(i) + "/" + str(j))
            i+= 1
            j+= 1
            k+= 1

    if k < len(s):
        encontrada=False
    return (encontrada)

#LIMITES DE LA MATRIZ SOPA
MF=100
MC=100


#PEDIMOS LAS DIMENSIONES DE LA MATRIZ SOPA HASTA QUE SEAN VALIDAD
N=int(input("Introduzca el número de FILAS de la SOPA: "))
while N<1 or N>MF:
    print ("Numero de filas introducido invalido,intente de nuevo")
    N=int(input("Introduzca el número de FILAS de la SOPA: "))

M=int(input("Introduzca el número de COLUMNAS de la SOPA: "))
while M<1 or M>MC:
    print ("Numero de columans introducido invalido,intente de nuevo")
    M=int(input("Introduzca el número de COLUMNAS de la SOPA: "))

#CREAMOS LA SOPA DE LETRAS
SOPA= arreglo.empty((N,M), dtype='object')
secuencia=""
cont_lineas=0

#LLENAMOS LA SOPA DE LETRAS
while cont_lineas < N:
    secuencia=input("Introduzca una secuencia de exactamente  " + str(M) + " caracteres sin espacios : ")
    if validar_secuencia(secuencia,M) == False:
        print ("Secuencia INVÁLIDA...")
    else:
        secuencia=secuencia.upper()
        i=0
        while i < M:
            SOPA[cont_lineas,i]=secuencia[i]
            i+=1
        cont_lineas+=1
print(SOPA)
MK=1000

#PEDIMOS EL NRO DE PALABRAS A BUSCAR HASTA QUE SEA VALIDO
K=int(input("Introduzca el número de PALABRAS a buscar en la SOPA: "))
while K<1 or K>MK:
    print ("Numero de palabras introducido invalido,intente de nuevo")
    K=int(input("Introduzca el número de PALABRAS a buscar en la SOPA: "))

contPalabra=0
palabra=""
palabra_mas_repetida = ""
nro_veces=0
#PEDIMOS LAS PALABRAS. CADA PALABRA LA BUSCAMOS EN LA SOPA
while contPalabra<K:
    palabra=input("Introduzca la palabara a buscar en la SOPA: ")
    palabra=palabra.upper()
    if validar_palabra(palabra) == False:
        print ("Palabra invalida , Intente de nuevo")
    else:
        contPalabra+=1
        caracterinicial=palabra[0]
        nro_apariciones=0
        encontrada=False
        i=0
        while i < N:
            j=0
            while j < M:
                if SOPA[i,j] == caracterinicial:
                    if len(palabra) > 1:
                        #ESTE: se deja la fila fija y se incrementa la columna
                        #print(str(i) + "/" + str(j))
                        if buscar_este(SOPA,palabra,i,j + 1)==True:
                            nro_apariciones+=1
                            print("Encontrada en dir ESTE en " + str(i) + "/" + str(j))
                            #CASO ESPECIAL: VAMOS A MOVER j hasta el fin de la palabra encontrada
                            #j=j + len(palabra) - 1
                        #OESTE : se deja la fila fija y se decrementa la columna
                        #print(str(i) + "/" + str(j))
                        if buscar_oeste(SOPA,palabra,i, j - 1)==True:
                            nro_apariciones+=1
                            print("Encontrada en dir OESTE en " + str(i) + "/" + str(j))
                        #NORTE: se deja la columna fija y se decrementa la fila
                        #print(str(i) + "/" + str(j))
                        if buscar_norte(SOPA,palabra,i-1,j)==True:
                            nro_apariciones+=1
                            print("Encontrada en dir NORTE en " + str(i) + "/" + str(j))
                        #SUR: se deja la columna fija y se incrementa la fila
                        #print(str(i) + "/" + str(j))
                        if buscar_sur(SOPA,palabra,i+1,j)==True:
                            nro_apariciones+=1
                            print("Encontrada en dir SUR en " + str(i) + "/" + str(j))

                        #NOROESTE: se decrementa tanto la fila como la columna
                        #print(str(i) + "/" + str(j))
                        if buscar_noroeste(SOPA,palabra,i-1,j-1)==True:
                            nro_apariciones+=1
                            print("Encontrada en dir NOROESTE en " + str(i) + "/" + str(j))
                        #NORESTE: se decrementa la fila y se incrementa la columna
                        #print(str(i) + "/" + str(j))
                        if buscar_noreste(SOPA,palabra,i-1,j+1)==True:
                            nro_apariciones+=1
                            print("Encontrada en dir NORESTE en " + str(i) + "/" + str(j))
                        #SUROESTE: se decremeenta la columna y se incrementa la fila
                        #print(str(i) + "/" + str(j))
                        if buscar_suroeste(SOPA,palabra,i+1,j-1)==True:
                            nro_apariciones+=1
                            print("Encontrada en dir SUROESTE en " + str(i) + "/" + str(j))
                        #SURESTE: se inrementa tanto la fila como la columna
                        #print(str(i) + "/" + str(j))
                        if buscar_sureste(SOPA,palabra,i+1,j+1) == True:
                            nro_apariciones+=1
                            print("Encontrada en dir SURESTE en " + str(i) + "/" + str(j))
                    else:
                        nro_apariciones+=1
                        print("Encontrada!!!")
                    encontrada=False
                j+=1
            i+=1
        print ("Nro de apariciones de la palabra " + palabra + "=" + str(nro_apariciones))
        if nro_apariciones > nro_veces:
            nro_veces = nro_apariciones
            palabra_mas_repetida = palabra
if nro_veces > 0:
    print ("La palabra que mas aparece es: " + palabra_mas_repetida + " (" + str(nro_veces) + ") VECES" )
else:
    print ("Ninguna palabra fue ENCONTRADA !!!")

